package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Gymnospermae_Manfaat extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_gymnospermae__manfaat, container, false);
		
		TextView teks1 = (TextView)rootView.findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("Bahan industri kertas, contohnya <i>Podocarpus, Pinus, Sequoia, dan Agathis.</i>"));
		
		TextView teks2 = (TextView)rootView.findViewById(R.id.teks2);
		teks2.setText(Html.fromHtml("Obat-obatan, contohnya <i>Ginkgo biloba</i> dan <i>Pinus</i> (getahnya untuk obat luka)."));
		
		TextView teks3 = (TextView)rootView.findViewById(R.id.teks3);
		teks3.setText(Html.fromHtml("Kosmetik, contohnya <i>Ginkgo biloba</i>, sebagai agen anti penuaan."));
		
		TextView teks4 = (TextView)rootView.findViewById(R.id.teks4);
		teks4.setText(Html.fromHtml("Bahan makanan, contohnya <i>Gnetum gnemon</i> (daunnya untuk sayuran dan bijinya untuk membuat emping)."));
		
		TextView teks5 = (TextView)rootView.findViewById(R.id.teks5);
		teks5.setText(Html.fromHtml("Tanaman hias, contohnya <i>Cycas, dioon edule,</i> dan <i>Cupressus.</i>"));
		
		TextView teks6 = (TextView)rootView.findViewById(R.id.teks6);
		teks6.setText(Html.fromHtml("Bahan kayu bangunan, contohnya <i>Podocarpus, Sequoia</i> (kayu merah), dan <i>Adathis</i> (untuk bahan kayu lapis atau tripleks)."));
		
		return rootView;
	}


}
