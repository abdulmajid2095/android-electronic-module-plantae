package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Spermatophyta_Ciri extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_spermatophyta__ciri, container, false);
		
		TextView teks1 = (TextView)rootView.findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("Berbatang pendek, merayap, berumpun. Contohnya rumput teki (<i>Cyperus rotundus</i>) dan serai (<i>Andropogon nardus</i>)."));
		
		TextView teks2 = (TextView)rootView.findViewById(R.id.teks2);
		teks2.setText(Html.fromHtml("Berbentuk seperti pohon tetapi batangnya kecil dan pendek. Contohnya bunga pukul empat (<i>Mirabilis jalapa</i>) dan cabai (<i>Capsicum frustecents</i>)."));
		
		TextView teks3 = (TextView)rootView.findViewById(R.id.teks3);
		teks3.setText(Html.fromHtml("Berbatang besar dan tinggi. Contohnya jambu air (<i>Eugenia aquea</i>) dan jati (<i>Tectona grandis</i>)."));
		
		TextView teks4 = (TextView)rootView.findViewById(R.id.teks4);
		teks4.setText(Html.fromHtml("Berbentuk seperti tali tambang dan tumbuh melilit pada pohon lain. Contohnya rotan (<i>Calamus rotang</i>) dan sirih (<i>piper betle</i>)."));
		
		return rootView;
	}


}
