package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Mono_2 extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_mono_2);
		
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		Button mono1=(Button)findViewById(R.id.mono_1);
		mono1.setOnClickListener(this);
		Button mono2=(Button)findViewById(R.id.mono_2);
		mono2.setOnClickListener(this);
		Button mono3=(Button)findViewById(R.id.mono_3);
		mono3.setOnClickListener(this);
		Button mono4=(Button)findViewById(R.id.mono_4);
		mono4.setOnClickListener(this);
		Button mono5=(Button)findViewById(R.id.mono_5);
		mono5.setOnClickListener(this);
		Button mono6=(Button)findViewById(R.id.mono_6);
		mono6.setOnClickListener(this);
		Button mono7=(Button)findViewById(R.id.mono_7);
		mono7.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.kembali:
			onBackPressed();
			break;
			
		case R.id.mono_1:
			Intent intent1 = new Intent(Mono_2.this,Mono_2a.class);
			startActivity(intent1);
			break;
		case R.id.mono_2:
			Intent intent2 = new Intent(Mono_2.this,Mono_2b.class);
			startActivity(intent2);
			break;
		case R.id.mono_3:
			Intent intent3 = new Intent(Mono_2.this,Mono_2c.class);
			startActivity(intent3);
			break;
		case R.id.mono_4:
			Intent intent4 = new Intent(Mono_2.this,Mono_2d.class);
			startActivity(intent4);
			break;
		case R.id.mono_5:
			Intent intent5 = new Intent(Mono_2.this,Mono_2e.class);
			startActivity(intent5);
			break;
		case R.id.mono_6:
			Intent intent6 = new Intent(Mono_2.this,Mono_2f.class);
			startActivity(intent6);
			break;
		case R.id.mono_7:
			Intent intent7 = new Intent(Mono_2.this,Mono_2g.class);
			startActivity(intent7);
			break;

		}
	}

	

}
