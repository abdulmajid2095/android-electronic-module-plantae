package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.text.Html;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class Paku_Lycopsida extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_paku__lycopsida);
		
		TextView teks1 = (TextView)findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("Sporangium menghasilkan spora. Lycopsida ada yang menghasilkan satu jenis spora (homospora), misalnya <i>Lycopodium sp.</i>, ada pula yang menghasilkan dua jenis spora (heterospora), misalnya <i>Selaginella sp.</i>"));
	}


}
