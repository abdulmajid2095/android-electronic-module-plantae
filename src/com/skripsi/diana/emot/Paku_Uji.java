package com.skripsi.diana.emot;

import java.util.ArrayList;
import java.util.List;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class Paku_Uji extends Fragment implements OnClickListener{

	Button jawab;
	LinearLayout utama;
	String[] jawaban={"a. Memiliki sistem vaskuler","b. Memiliki rizom","c. Salah satu habitatnya epifit","d. sudah memiliki akar","e. Ukuran generasi sporofit lebih kecil",
			"a. Keduanya memiliki sistem vaskuler","b. Tumbuhan paku memiliki akar dan lumut tidak","c. Tumbuhan lumut memiliki xilem, sedang paku tidak","d. Tumbuhan paku memiliki rizoid","e. Fase sporofit dominan pada tumuhan paku",
			"a. Menghasilkan satu spora yang sama ukuran  dan bentuknya","b. Menghasilkan dua spora dengan ukuran dan jenis yang berbeda","c. Menghasilkan spora yang berukuran sama tetapi jenisnya berbeda","d. Menghasilkan gamet yang sama","e. Menghasilkan gamet yang berbeda",
			"a. Mikrofil","b. Makrofil","c. Tropofil","d. Sporofil","e. Gametofit",
			"a. Adiantum sp (paku suplir)","b. Asplenium nidus (paku tanduk rusa)","c. Lycopodium sp","d. Azolla pinnata (paku sampan)","e. Alsophila galuca (paku tiang)",
			"a. Psiolopsida","b. Lycopsida","c. Equisetopsida","d. Pteropsida","e. Hepaticopsida",
			"a. Selaginella sp","b. Alsophila glauca","c. Equisetum sp","d. Marsilea crenata","e. Lycopodium sp",
			"a. Psilopsida, Equisetopsida, Pteropsida, Lycopsida","b. Pteropsida, Anthoceropsida, Lycopsida, Equisetopsida","c. Antoceropsida, Lycopsida, Bryopsida, Pteropsida","d. Anthoceropsida, Bryopsida, Pteropsida, Equisetopsida","e. Lycopsida, Hepaticopsida, Pteropsida, Equisetopsida",
			"a. Anteridium menghasilkan spermatozoid","b. Zigot yang tumbuh menjadi tumbuhan paku","c. Protalium membentuk anteridium dan arkegonium","d. Sprorofit dewasa menghasilkan sporofil","e. Sel induk spora menghasilkan spora haploid",
			"a. Indusium","b. Sporangiofor","c. Sporangia","d. Induk spora","e. Sporogonium",
			"a. Protalium","b. Tumbuhan paku","c. Spora","d. Arkegonium","e. Anteridium",
			"a. Selaginella","b. Lycopodium","c. Nephrolepis","d. Drymoglosum","e. Equisetum",
			"a. Spora kromosom haploid apabila jatuh ditempat lembab akan tumbuh dan berkembang membentuk  protonema","b. Anteridium menghasilkan spermatozoid yang bersifat diploid","c. Zigot mengalami pembelahan secara meiosis menjadi tumbuhan paku","d. Tumbuhan paku dewasa menghasilkan sporofil bersifat diploid","e. Sporofit memiliki sporangium yang di dalamnya terdapat sel induk spora haploid",
			"a. Makrospora","b. Mikrospora","c. Protalium","d. Megaprotalium","e. Mikroprotalium",
			"a. Salvinia molesta","b. Equisetum","c. Selaginella palma","d. Alsophila glauca","e. Neprolepis",
};
	
	
	
	String[] soal={"1.	Berikut ini merupakan ciri-ciri tumbuhan paku, kecuali �",
					"2.	Salah satu perbedaan tumbuhan lumut dengan tumbuhan paku yaitu �",
					"3.	Marsilea crenata merupakan paku heterospora karena �",
					"4.	Daun pada tumbuhan paku yang berfungsi untuk fotosintesis adalah �",
					"5. Manfaat paku untuk pupuk hijau antara lain �",
					"6. Saat pergi ke sekolah, kiki menjumpai tanaman paku yang beradada di dekat gundukan pasir dengan ciri-ciri:\n1) Percabangan batang berbentuk ulir\n2) Batang keras dan berongga\n3) Sporofitnya berbentuk sisik\n4) Batang menyerupai ekor kuda\nBerdasarkan ciri-ciri tumbuhan paku diatas, dapat dimasukan kedalam �",
					"7. Tumbuhan paku yang mengandung silika dan dimanfaatkan sebagai bahan penggosok (amplas) adalah �",
					"8. Klasifikasi tumbuhan paku dibagi menjadi 4 kelas yaitu �",
					"9. Pada tumbuhaan paku fase pembelahan meiosis terjadi apabila �",
					"10. Kumpulan sporangium  paku dalam suatu badan/tempat disebut �",
					"11. Pada tumbuhan paku, bagian yang memiliki jumlah kromosom 2n (diploid) adalah �",
					"12. Salah satu contoh tumbuhan paku heterospora adalah �",
					"13. Manakah pernyataan berikut yang benar mengenai metagensis tumbuhan paku �",
					"14. Arkegonium paku peralihan berasal dari �",
					"15. Tumbuhan paku selain memguntungkan manusia, ada juga yang merugikan bagi manusia. Berikut ini tumbuhan paku yang merugikan bagi manusia adalah �"
	};
	
	String[] kunci={"e","b","b","c","d",
					"c","c","a","e","e",
					"b","a","d","c","a"};
	
	
	int benar=0;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_paku__uji, container, false);
		
		utama = (LinearLayout)rootView.findViewById(R.id.layout_uji);
		
		
		jawab = (Button)rootView.findViewById(R.id.uji_tombol_jawab);
		jawab.setOnClickListener(this);
		
		looping();
		
		
		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.uji_tombol_jawab:
			int skorku = (benar+5)*5;
			Dataku data = Dataku.findById(Dataku.class, 1L);
			if(skorku>data.skor_paku)
			{
				data.skor_paku = skorku;
				data.save();
			}
			
			Intent intent = new Intent(getActivity(),Skor.class);
			intent.putExtra("pilihan", "paku");
			intent.putExtra("skor", ""+skorku);
			startActivity(intent);
			break;
			
		}
	}
	
	public void looping()
	{
		for(int l=0;l<15;l++)
		{	
		
		//Soal
		TextView namawisata = new TextView(getActivity());
		namawisata.setText(""+soal[l]);
		namawisata.setTextSize(15);
		LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT );
		namawisata.setLayoutParams(params1);
		
		final RadioGroup rg = new RadioGroup(getActivity());
		rg.setId(l);
		int n1=l+1;
		int n2=n1*5;
		int n3=n2-5;
		
        for(int i=0;i<5;i++){
            RadioButton rb=new RadioButton(getActivity()); // dynamically creating RadioButton and adding to RadioGroup.
            //rb.setId(R.id.);
            rb.setText(""+jawaban[n3+i]);
            rg.addView(rb);
        }
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				int childCount = group.getChildCount();
                for (int x = 0; x < childCount; x++) {
                   RadioButton btn = (RadioButton) group.getChildAt(x);
                   if (btn.getId() == checkedId) {
                	   
                	   String ans = btn.getText().toString();
                	   String ans2 = ans.subSequence(0, 1).toString();
                	   int d = rg.getId();
                	   //Toast.makeText(getActivity(), ""+d, Toast.LENGTH_LONG).show();
                	   if(ans2.equals(kunci[d]))
                	   {
                		   benar=benar+1;
                	   }

                   }
                }
			}
		});
        
        LinearLayout lay1 = new LinearLayout(getActivity());
        lay1.setBackgroundColor(Color.rgb(65, 148, 92));
		LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,2);
		params2.bottomMargin=10;
		params2.topMargin=10;
		lay1.setLayoutParams(params2);
       
		
		utama.addView(namawisata);
		utama.addView(rg);
		utama.addView(lay1);
		
		}
	}


}
