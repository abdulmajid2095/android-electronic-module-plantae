package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Paku_Klasifikasi extends Fragment implements OnClickListener{
	Button detail_philopsida,detail_lycopsida,detail_spenopsida,detail_pteropsida;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_paku__klasifikasi, container, false);
		detail_philopsida = (Button) rootView.findViewById(R.id.paku_detail_philopsida);
		detail_philopsida.setOnClickListener(this);
		detail_lycopsida = (Button) rootView.findViewById(R.id.paku_detail_lycopsida);
		detail_lycopsida.setOnClickListener(this);
		detail_spenopsida = (Button) rootView.findViewById(R.id.paku_detail_spenopsida);
		detail_spenopsida.setOnClickListener(this);
		detail_pteropsida = (Button) rootView.findViewById(R.id.paku_detail_pteropsida);
		detail_pteropsida.setOnClickListener(this);
		return rootView;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.paku_detail_lycopsida:
			Intent intent1 = new Intent(getActivity(),Paku_Lycopsida.class);
			startActivity(intent1);
			break;
		case R.id.paku_detail_philopsida:
			Intent intent2 = new Intent(getActivity(),Paku_Philopsida.class);
			startActivity(intent2);
			break;
		case R.id.paku_detail_pteropsida:
			Intent intent3 = new Intent(getActivity(),Paku_Pteropsida.class);
			startActivity(intent3);
			break;
		case R.id.paku_detail_spenopsida:
			Intent intent4 = new Intent(getActivity(),Paku_Spenopsida.class);
			startActivity(intent4);
			break;

		}
	}


}
