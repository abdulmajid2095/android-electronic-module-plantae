package com.skripsi.diana.emot;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.VideoView;

public class Lumut_Video extends Fragment implements OnClickListener{


	Button play;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_lumut__video, container, false);
		
		
		
		play = (Button) rootView.findViewById(R.id.play);
		play.setOnClickListener(this);
		
		
		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
			case R.id.play:
				
				Intent intent = new Intent(getActivity(),Video_Lumut.class);
				startActivity(intent);
				
			break;
			
		}
	}
	
	


}
