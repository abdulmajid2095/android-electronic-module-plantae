package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class Materi_Jenis extends Activity implements OnClickListener{

	Button detail_paku,detail_biji;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_materi__jenis);
		
		detail_biji = (Button) findViewById(R.id.materi_sper);
		detail_biji.setOnClickListener(this);
		detail_paku=(Button)findViewById(R.id.materi_paku);
		detail_paku.setOnClickListener(this);
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		
		case R.id.materi_paku:
			Intent paku = new Intent(Materi_Jenis.this,Materi3.class);
			startActivity(paku);
			break;
			
		case R.id.materi_sper:
			Intent spermatophyta = new Intent(Materi_Jenis.this,Materi4.class);
			startActivity(spermatophyta);
			break;
		case R.id.kembali:
			onBackPressed();
			break;

		}
		
	}

}
