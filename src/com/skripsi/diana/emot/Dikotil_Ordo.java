package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Dikotil_Ordo extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_dikotil__ordo, container, false);
		
		TextView teks1 = (TextView)rootView.findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("Casuarinaceae berbentuk pohon, berumah satu atau dua, memiliki ranting jarum yang hijau dengan sendi antar ruasyang beralur. Daun casuarinaceae tereduksi, bunga dalam bulir berbentuk kerucut  dan buah bonggol berbentuk kerucut. Contohnya adalah <i>Casuarina equisetifolia</i> (cemara laut) dan <i>Casuarina junghuhniana</i> (cemara gunung)."));
		
		TextView teks2 = (TextView)rootView.findViewById(R.id.teks2);
		teks2.setText(Html.fromHtml("Caparaceae berbentuk perdu, pohon, atau liana berkayu. Daunnya tunggal atau majemuk manjari, dan berukuran kecil. Buah berbentuk kapsul memnajang (disebut buah buni). Contohnya <i>Gynandropsis speciosa</i> dan <i>Capparis spinosa.</i>"));
		
		TextView teks3 = (TextView)rootView.findViewById(R.id.teks3);
		teks3.setText(Html.fromHtml("Malvaceae berbentuk perdu atau pohon. Daunnya tungal menjari atau berurat daun menjari di bagian pangkal. Bunganya memiliki 5 daun kelopak dan 5 daun mahkota, bekelamin dua, benag sari banyak, tangkai sari bersatu, dan tangkai putik berada di atasnya. Contohnya : kembang sepatu (<i>Hibiscus rosa-sinensis</i>),kapas (<i>Gossypium sp</i>), dan <i>Abutilon sp.</i>"));
		
		TextView teks4 = (TextView)rootView.findViewById(R.id.teks4);
		teks4.setText(Html.fromHtml("Myrtaceae berbentuk pohon atau perdu. Daunnya tampak selalu hijau dan beraroma jika diremas. Contohnya:  <i>Eucalyptus</i> dan <i>Eugenia caryophyllus</i> (cengkih)."));
		
		TextView teks5 = (TextView)rootView.findViewById(R.id.teks5);
		teks5.setText(Html.fromHtml("Leguminosae berbentuk perdu atau pohon, ada pula yang menjat. Leguminosae memiliki daun buah menjang yang akan berkembang menjadi polong (legum). Sebagian leguminosae memiliki bintil pada akar yang meurpakan bentuk simbiosis dengan bakteri penambat nitrogen (<i>Rhizobium sp</i>). Leguminosae terdiriatas tiga sub famili yaitu:"));
		
		TextView teks6 = (TextView)rootView.findViewById(R.id.teks6);
		teks6.setText(Html.fromHtml("Contohnya <i>mimosa pudica</i> (putri malu), <i>Leucaena leucocephala</i> (petai cina)."));
		
		TextView teks7 = (TextView)rootView.findViewById(R.id.teks7);
		teks7.setText(Html.fromHtml("Contohnya, <i>Caesalpinia pulcherrima</i> (bunga merak), <i>Delonix regia</i> (flamboyan)."));
		
		TextView teks8 = (TextView)rootView.findViewById(R.id.teks8);
		teks8.setText(Html.fromHtml("Contohnya <i>Arachis hypogea</i> (kacang tanah) dan <i>Crotalaria juncea</i> (orok-orok)."));
		
		TextView teks9 = (TextView)rootView.findViewById(R.id.teks9);
		teks9.setText(Html.fromHtml("Apocynaceae berbentuk pohon, perdu atau liana berkayu. Batangnya bergetah putih. Pada umumnya memiliki bunga dengan warna mencolok, berukuran besar, dan berbau harum. Contohnya: <i>catharanthus rosesu</i> (tapak dara) dan <i>Allamanda cathartica</i> (alamanda)."));
		
		TextView teks10 = (TextView)rootView.findViewById(R.id.teks10);
		teks10.setText(Html.fromHtml("Compositae berbentuk perdu atau pohon. Bunganya memiliki bonggol berbentuk tabung. Contohnya <i>Lactuca sativa</i> (selada) dan <i>Chrysanthenum</i>."));
		
		TextView teks11 = (TextView)rootView.findViewById(R.id.teks11);
		teks11.setText(Html.fromHtml("Piperaceae berbentuk perdu atau semak, ada yang memanjat dengan akar lekat. Daun memiliki bau aromatik atau rasa pedas. Contohnya: <i>Piper betle</i> (sirih) dan <i>Piper nigrum</i> (lada)."));
		
		TextView teks12 = (TextView)rootView.findViewById(R.id.teks12);
		teks12.setText(Html.fromHtml("Rosaceae merupakan kelompok mawar, berbentuk semak namun ada pula yang memanjat, berkayu, berduri tempel atau tidak berduri. Contohnya: <i>Rosa hybrida</i> (mawar) dan <i>Malus sylvestris</i> (apel)."));
		
		TextView teks13 = (TextView)rootView.findViewById(R.id.teks13);
		teks13.setText(Html.fromHtml("Solanceae merupakan kelompok terong-terongan. Berbentuk perdu atau semak basah. Bunganya berbentuk terompet. Contohnya <i>Datura metel</i> (kecubung) dan <i>Solanum lycopersicum</i> (tomat)."));
		
		TextView teks14 = (TextView)rootView.findViewById(R.id.teks14);
		teks14.setText(Html.fromHtml("Magnoliaceae berbentuk pohon atau perdu. Daun tunggal dan pada saat rontok meninggalkan bekas berbentuk cincin pada ranting. Kelopak dan mahkota tidak selalu dapat dibedakan dengan jelas. Contohnya <i>Michelia champaka</i> (cempaka atau kantil)."));
		
		TextView teks15 = (TextView)rootView.findViewById(R.id.teks15);
		teks15.setText(Html.fromHtml("Nyctaginaceae berbentuk pohon, perdu atau memanjat berdaun tunggal; ada yang memiliki daun pelindung berwarna hijau atau warna lainnya. Contohnya <i>Bougainvillea spectabilis</i> dan <i>Mirabilis jalapa</i> (bunga pukul empat)."));
		
		TextView teks16 = (TextView)rootView.findViewById(R.id.teks16);
		teks16.setText(Html.fromHtml("Nymphaeaceae merupakan tumbuhan air atau rawa. Daqun tenggelam atau mengapung. Contohnya <i>Nymphaea nouchali</i> (teratai kecil)dan <i>Nelumbium nelumbo</i>."));
		
		TextView teks17 = (TextView)rootView.findViewById(R.id.teks17);
		teks17.setText(Html.fromHtml("Rutaceae berbentuk pohon atau perdu. Daun memiliki kelenjar minyak. Contohnya <i>Citrus maxima</i> (jeruk bali) dan <i>Murraya paniculata</i> (kemuning)."));
		
		
		return rootView;
	}


}
