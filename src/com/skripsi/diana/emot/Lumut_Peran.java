package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Lumut_Peran extends Fragment {

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_lumut__peran, container, false);
		
		TextView teks1 = (TextView)rootView.findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("1. <i> Marchantia  polymorpha </i> sebagai obat hepatitis"));
		
		TextView teks2 = (TextView)rootView.findViewById(R.id.teks2);
		teks2.setText(Html.fromHtml("2. <i>Spagnum</i> untuk bahan pembalut dn bahan bakar"));
		return rootView;
	}


}
