package com.skripsi.diana.emot;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import android.R.integer;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Game_b extends Activity implements OnClickListener{

	String[]soal={"Daun pada paku yang menghasilkan spora",	
		"Bagian x lumut yang ditunjuk oleh anak panah",
		"Metagenesis paku yang menghasilkan dua jenis spora yang berbeda ukuran",
		"Kelas Cycas rumpii",
		"Spesies Gnetinae yang dapat dimanfaatkan sebagai bahan makanan",
		"Kelas pada tumbuhan lumut dengan ciri: sporofit memanjang seperti tanduk dan mengandung kutikula",
		"Tumbuhan spermatophyta yang dimanfaatkan untuk bahan makanan pokok",
		"Spesies paku ekor kuda",
		"Bagian yang ditunjuk oleh tanda x",
		"Gametofit pada tumbuhan paku",
		"Sifat anteridium dan arkegonium pada tumbuhan paku",
		"Tanaman dengan akar serabut",
		"Spesies Spermatophyta yang dimanfaatkan sebagai bangunan kayu",
		"Tumbuhan air yang termasuk ordo Nymphaeales dengan ciri memiliki daun tipis, mengapung, batang berongga dan biasa hidup di air atau rawa-rawa",
		"Pembentukan endospermae terjadi bila inti sperma membuah"
		
	};
	int[]gambar={R.drawable.game1,
			R.drawable.game2,
			R.drawable.game3,
			R.drawable.game4,
			R.drawable.game5,
			R.drawable.game6,
			R.drawable.game7,
			R.drawable.game8,
			R.drawable.game9,
			R.drawable.game10,
			R.drawable.game11,
			R.drawable.game12,
			R.drawable.game13,
			R.drawable.game14,
			R.drawable.game15
	};
	String[]clue={"_ P _ R _ F _ L",
			"S _ _ R _ _ G _ _ M",
			"P _ K _   H _ _ E _ O _ P _ _ _",
			"C _ _ A _ _ N _ E",
			"G _ _ T _ M   G _ _ M _ _",
			"A _ _ H _ _ E _ _ P _ _ D _",
			"_ R _ _ A   _ _ T _ _ A",
			"_ Q _ _S _ _ _ M   _ P",
			"R _ _ O _ D",
			"P _ _ T _ _ L _ _ M",
			"D _ _ L _ _ D",
			"_ O _ _ K _ _ _ L",
			"T _ _ T _ _ A   _ R _ _ D _ _",
			"N _ _ _ M _ _ U _   _ E _ U _ _ O",
			"_ N _ I   _ A _ D _ _ G   L _ _ B _ G _   _ _ K _ _ D _ R"
			
			
	};
	String[]jawaban={"sporofil",
			"sporangium",
			"paku heterospora",
			"cycadinae",
			"gnetum gnemon",
			"anthoceropsida",
			"oryza sativa",
			"equisetum sp",
			"rizoid",
			"protallium",
			"diploid",
			"monokotil",
			"tectona grandis",
			"nelumbium nelumbo",
			"inti kandung lembaga sekunder"
	};
	String level;
	ImageView gambar_soal,nyawa1,nyawa2,nyawa3;
	TextView teks_soal,teks_clue;
	EditText input;
	Button jawab;
	int levelmu,nyawa=3;
	Dataku data;
	MediaPlayer musik7;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(
				Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_game_b);
		
		Intent baru = getIntent();
		level = baru.getStringExtra("level");
		int levelku = Integer.parseInt(level);
		levelmu = levelku-1;
		
		gambar_soal = (ImageView) findViewById(R.id.game_b_gambar);
		teks_soal = (TextView)findViewById(R.id.game_b_soal);
		teks_clue = (TextView)findViewById(R.id.game_b_clue);
		input = (EditText)findViewById(R.id.game_b_edittext);
		jawab=(Button)findViewById(R.id.game_b_jawab);
		jawab.setOnClickListener(this);
		
		nyawa1=(ImageView)findViewById(R.id.nyawa1);
		nyawa2=(ImageView)findViewById(R.id.nyawa2);
		nyawa3=(ImageView)findViewById(R.id.nyawa3);
		
		data = Dataku.findById(Dataku.class, 1L);
		
		set_soal();
		
		musik7 = MediaPlayer.create(this, R.raw.school);
		musik7.setLooping(true);
        musik7.setVolume(1,1);
        musik7.start();
        
        Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
        YoYo.with(Techniques.BounceIn).duration(3000).playOn(findViewById(R.id.animasi_1));
		
		}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.game_b_jawab:
			cek_jawaban();
			break;
		case R.id.kembali:
			onBackPressed();
			break;
		}
	}
	
	public void set_soal()
	{
		gambar_soal.setImageResource(gambar[levelmu]);
		teks_soal.setText(""+soal[levelmu]);
		teks_clue.setText(""+clue[levelmu]);
	}
	
	public void cek_jawaban()
	{
		String jawab_user = input.getText().toString();
		String jawab_user2 = jawab_user.toLowerCase();
		String cek = "";
		if(jawab_user2.equals(cek))
		{
			dialog_kosong();
		}
		else
		{
			if(jawab_user2.equals(jawaban[levelmu]))
			{
				benar();
			}
			else
			{
				nyawa = nyawa-1;
				set_nyawa();
				if(nyawa==0)
				{
					game_over();
				}
			}
		}
		
	}

	public void set_nyawa()
	{
		MediaPlayer h4 = MediaPlayer.create(this, R.raw.dor);
        h4.setVolume(1,1);
        h4.start();
        
        
		if(nyawa==2)
		{
			nyawa3.setVisibility(View.INVISIBLE);
		}
		else if(nyawa == 1)
		{
			nyawa2.setVisibility(View.INVISIBLE);
		}
		else if(nyawa ==0)
		{
			nyawa1.setVisibility(View.INVISIBLE);
		}
	}
	
	public void game_over()
	{
		musik7.stop();
		musik7.release();
		musik7 = null;
		MediaPlayer h4 = MediaPlayer.create(this, R.raw.over);
        h4.setVolume(1,1);
        h4.start();
        
		if(data.level>3)
		{
			data.level=data.level-3;
			data.save();
		}
		else
		{
			data.level=1;
			data.save();
		}
		
		final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_salah);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        ImageButton tombol = (ImageButton) dialog.findViewById(R.id.tombol);
        tombol.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(Game_b.this,Game_a.class);
				startActivity(intent);
			}
		});
        dialog.show();
        YoYo.with(Techniques.Swing).duration(3000).playOn(dialog.findViewById(R.id.tombol));
	}
	
	public void benar()
	{
			if(data.level<15)
			{
				data.level=data.level+1;
				data.save();
			}
			
	        // custom dialog
	        final Dialog dialog = new Dialog(this);
	        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        dialog.setContentView(R.layout.dialog_benar);
	        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
	        dialog.setCancelable(false);
	        ImageButton tombol = (ImageButton) dialog.findViewById(R.id.tombol);
	        tombol.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					musik7.stop();
					musik7.release();
					musik7 = null;
					Intent intent = new Intent(Game_b.this,Game_a.class);
					startActivity(intent);
				}
			});
	        dialog.show();
	        YoYo.with(Techniques.RubberBand).duration(3000).playOn(dialog.findViewById(R.id.tombol));

	    
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		musik7.stop();
		musik7.release();
		musik7 = null;
		Intent intent = new Intent(Game_b.this,Game_a.class);
		startActivity(intent);
	}
	
	public void dialog_kosong()
	{
			AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
			dialogkeluar.setTitle("Error");
			dialogkeluar.setMessage("Jawaban tidak boleh kosong");
			dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which){
					dialog.dismiss();
				}
			});
			dialogkeluar.show();
	    
	}

}
