package com.skripsi.diana.emot;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

public class Video_Gymnospermae extends Activity {

	VideoView v3;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		setContentView(R.layout.activity_video__gymnospermae);
		
		v3 = (VideoView)findViewById(R.id.v3);
		Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.v3);
		v3.setVideoURI(uri);
		v3.setMediaController(new MediaController(this));
		v3.requestFocus();
		v3.start();
	}

	

}
