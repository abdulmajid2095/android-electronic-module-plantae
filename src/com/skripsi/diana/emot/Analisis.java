package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.text.Html;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Analisis extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_analisis);
		
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		Button a1 = (Button) findViewById(R.id.analisis_1);
		a1.setOnClickListener(this);
		Button a2 = (Button) findViewById(R.id.analisis_2);
		a2.setOnClickListener(this);
		
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.kembali:
			onBackPressed();
			break;
		case R.id.analisis_1:
			Intent intent1 = new Intent(Analisis.this,Fenetik.class);
			startActivity(intent1);
			break;
			
		case R.id.analisis_2:
			Intent intent2 = new Intent(Analisis.this,Filogenetik.class);
			startActivity(intent2);
			break;

		}
		
		
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		Intent intent = new Intent(Analisis.this,MenuUtama.class);
		startActivity(intent);
	}
	

}
