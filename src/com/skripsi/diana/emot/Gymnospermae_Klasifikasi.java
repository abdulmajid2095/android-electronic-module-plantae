package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Gymnospermae_Klasifikasi extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_gymnospermae__klasifikasi, container, false);
		
		TextView teks1 = (TextView)rootView.findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("Cycadinae termasuk tumbuhan berumah dua karena mikropsora dan megaspora dihasilkan oleh tumbuhan yang berbeda. Mikrospora dihasilkan oleh tumbuhan jantan, sedangkan megaspora dihasilkan oleh tumbuhan betina. Contoh Cycadinae antara lain: <i>Cycas rumphii</i> (pakis haji), <i>Cycas revoluta, Dioon edule,</i> dan <i>Zamia floridana</i>."));
		
		TextView teks2 = (TextView)rootView.findViewById(R.id.teks2);
		teks2.setText(Html.fromHtml("Coniferae pada umumnya berumah satu karena memiliki dua jenis konus: jantan dan betina. Konus jantan berukuran lebih kecil daibandingkan konus betina. Konus jantan tumbuh secara bergerombol. Contoh konifer antara lain <i>Pinus mercusii, Texus baccata, Agathis dammara,</i> dan <i>Podocarpus neriifolius.</i>"));
		
		TextView teks3 = (TextView)rootView.findViewById(R.id.teks3);
		teks3.setText(Html.fromHtml("Contoh Gnetinae antara lain: <i>Gnetum gnemon</i> (melinjo), <i>Ephedra sinica,</i> dan <i>Welwitschia mirabilis.</i>"));
		
		TextView teks4 = (TextView)rootView.findViewById(R.id.teks4);
		teks4.setText(Html.fromHtml("Ginkgoinae merupakan tumbuhan berumah dua. Tumbuhan tersebut dapat bertahan hidup pada lingkungan dengan tingkat polusi udara tinggi. Contoh Ginkgoinae antara lain <i>Ginkgo bibola</i>. <i>Ginkgo adiantoides</i> dan <i>Ginkgo gardneri</i> merupakan jenias Ginkgoinae yang sudah punah."));
		
		return rootView;
	}


}
