package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Angio_3 extends Activity implements OnClickListener{

	Button diko,mono;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_angio_3);
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
		diko = (Button) findViewById(R.id.angio_diko);
		diko.setOnClickListener(this);
		mono = (Button) findViewById(R.id.angio_mono);
		mono.setOnClickListener(this);
		
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		/*case R.id.lanjut:
			Intent kingdom = new Intent(Materi.this,Materi_Jenis.class);
			startActivity(kingdom);
			break;*/
		case R.id.angio_diko:
			Intent kingdom = new Intent(Angio_3.this,Angio_3b.class);
			startActivity(kingdom);
			break;
		case R.id.angio_mono:
			Intent kingdom2 = new Intent(Angio_3.this,Angio_3a.class);
			startActivity(kingdom2);
			break;
		case R.id.kembali:
			onBackPressed();
			break;

		}
	}
	

}
