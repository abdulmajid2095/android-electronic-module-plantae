package com.skripsi.diana.emot;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

public class Game_Petunjuk extends Activity implements OnClickListener{

	ImageButton ok;
	MediaPlayer musik5;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_game__petunjuk);
		
		ok =(ImageButton)findViewById(R.id.game_petunjuk_ok);
		ok.setOnClickListener(this);
		
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
		musik5 = MediaPlayer.create(this, R.raw.chiptune2);
		musik5.setLooping(true);
        musik5.setVolume(1,1);
        musik5.start();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.game_petunjuk_ok:
			musik5.stop();
			musik5.release();
			musik5 = null;
			Intent intent = new Intent(Game_Petunjuk.this,Game_a.class);
			startActivity(intent);
			break;
		case R.id.kembali:
			onBackPressed();
			break;

	
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		musik5.stop();
		musik5.release();
		musik5 = null;
		Intent intent = new Intent(Game_Petunjuk.this,MenuUtama.class);
		startActivity(intent);
	}

}
