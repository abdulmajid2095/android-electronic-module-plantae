package com.skripsi.diana.emot;



import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class Adapter_lumut_tab extends FragmentPagerAdapter {

	public Adapter_lumut_tab(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int index) {
		switch (index) {
        case 0:
            // Top Rated fragment activity
            return new Lumut_Pengertian();
        case 1:
            // Games fragment activity
        	return new Lumut_Struktur();
        case 2:
            // Games fragment activity
        	return new Lumut_Perkembangan();
        case 3:
            // Games fragment activity
        	return new Lumut_Jenis();
        case 4:
            // Games fragment activity
        	return new Lumut_Peran();
        case 5:
            // Games fragment activity
        	return new Lumut_Video();
        case 6:
            // Games fragment activity
        	return new Lumut_Uji();
        }
		
 
        return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 7;
	}
	
	@Override
    public CharSequence getPageTitle(int position) {
        //return "Page #" + ( position + 1 );
    	switch (position) {
        case 0:
            // Top Rated fragment activity
        	return "Tentang Lumut";
        case 1:
            // Games fragment activity
        	return "Struktur dan Fungsi";
        case 2:
            // Games fragment activity
        	return "Perkembangan";
        case 3:
            // Games fragment activity
        	return "Kelas Lumut";
        case 4:
            // Games fragment activity
        	return "Peran Lumut";
        case 5:
            // Games fragment activity
        	return "Video";
        case 6:
            // Games fragment activity
        	return "Uji Kompetensi";
        }
 
        return null;
    }

}
