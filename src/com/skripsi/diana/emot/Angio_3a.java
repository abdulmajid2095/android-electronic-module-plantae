package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Angio_3a extends Activity implements OnClickListener{

	Button mono_1,mono_3;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_angio_3a);
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		mono_1=(Button)findViewById(R.id.mono__x_ciri);
		mono_1.setOnClickListener(this);
		mono_3=(Button)findViewById(R.id.mono_x_klasifikasi);
		mono_3.setOnClickListener(this);
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		
		case R.id.mono__x_ciri:
			Intent intent1 = new Intent(Angio_3a.this,Mono_1.class);
			startActivity(intent1);
			break;
		case R.id.mono_x_klasifikasi:
			Intent intent3 = new Intent(Angio_3a.this,Mono_2.class);
			startActivity(intent3);
			break;
		
			
		case R.id.kembali:
			onBackPressed();
			break;

		}
		
	}

}
