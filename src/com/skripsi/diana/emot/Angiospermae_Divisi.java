package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class Angiospermae_Divisi extends Fragment implements OnClickListener{

	Button detail_mono,detail_dico;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_angiospermae__divisi, container, false);
		detail_mono = (Button) rootView.findViewById(R.id.angiospermae_divisi_mono);
		detail_mono.setOnClickListener(this);
		detail_dico = (Button) rootView.findViewById(R.id.angiospermae_divisi_dico);
		detail_dico.setOnClickListener(this);
		return rootView;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.angiospermae_divisi_dico:
			Intent intent1 = new Intent(getActivity(),Materi_Dikotil.class);
			startActivity(intent1);
			break;
		case R.id.angiospermae_divisi_mono:
			Intent intent2 = new Intent(getActivity(),Materi_Monokotil.class);
			startActivity(intent2);
			break;

		}
	}


}
