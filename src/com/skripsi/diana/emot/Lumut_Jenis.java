package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class Lumut_Jenis extends Fragment implements OnClickListener {

	Button detail_daun,detail_hati,detail_tanduk;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_lumut__jenis, container, false);
		
		detail_daun = (Button) rootView.findViewById(R.id.lumut_detail_daun);
		detail_daun.setOnClickListener(this);
		detail_hati = (Button) rootView.findViewById(R.id.lumut_detail_hati);
		detail_hati.setOnClickListener(this);
		detail_tanduk = (Button) rootView.findViewById(R.id.lumut_detail_tanduk);
		detail_tanduk.setOnClickListener(this);
		
		return rootView;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.lumut_detail_daun:
			Intent daun = new Intent(getActivity(),Lumut_Daun.class);
			startActivity(daun);
			break;
		case R.id.lumut_detail_tanduk:
			Intent tanduk = new Intent(getActivity(),Lumut_Tanduk.class);
			startActivity(tanduk);
			break;
		case R.id.lumut_detail_hati:
			Intent hati = new Intent(getActivity(),Lumut_Hati.class);
			startActivity(hati);
			break;

		}
	}


}
