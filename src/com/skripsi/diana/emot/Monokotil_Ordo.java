package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Monokotil_Ordo extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_monokotil__ordo, container, false);
		
		TextView teks1 = (TextView)rootView.findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("Liliaceae merupakan semak basah, ada yang memanjat: memiliki akar rimpang, umbi, atau umbi lapis. Contohnya <i>Lilium regale</i> (bunga lili ) dan bunga tulip."));
		
		TextView teks2 = (TextView)rootView.findViewById(R.id.teks2);
		teks2.setText(Html.fromHtml("Amaryllidaceae merupakan semka basah menahun, memiliki umbi, umbi lapis, atau akar rimpang. Contohnya  <i>Polianthes tuberosa</i> (bunga sedap malam) dan <i>Zephyranthes rosea</i> (kembang cokelat)."));
		
		TextView teks3 = (TextView)rootView.findViewById(R.id.teks3);
		teks3.setText(Html.fromHtml("Orchidaceae merupakan kelompok anggrek yang merupakan tumbuhan semak menahun. Sebagian Orchidaceae hidup epifit, memiliki akar rimpang, dan memiliki daun berdaging. Contohnya <i>Vanda tricolor</i> dan <i>Spathoglottis plicata</i> (anggrek tanah)."));
		
		TextView teks4 = (TextView)rootView.findViewById(R.id.teks4);
		teks4.setText(Html.fromHtml("Palmae berbentuk pohon atau menajat. Padabatangterdapat bekas daun berbentuk cintin. Daun palmae menyirirp atau berbentuk kipas, dengan pangkal pelepah daun yang melebar. Contohnya <i>Metroxylon sagu</i> (sagu) dan <i>Cocos nucifera</i> (kelapa)."));
		
		TextView teks5 = (TextView)rootView.findViewById(R.id.teks5);
		teks5.setText(Html.fromHtml("Gramineae merupakan kelompok rumput-rumputan. Gramineae memiliki batang silindris, agak pipih, persegi dan berongga:;berdaun tunggal dan berpelepah; daun dan bunga tersusun dalam bulir; berbiji satu; dan batang berbuku-buku. Contohnya <i>Imperata cylindrica</i> (alang-alang) dan <i>Oryza sativa</i> (padi)"));
		
		TextView teks6 = (TextView)rootView.findViewById(R.id.teks6);
		teks6.setText(Html.fromHtml("Bromeliaceae termasuk kelompok nanas-nanasan yang berbentuk semak basah. Contohnya <i>Ananas comosus</i> (nanas)."));
		
		TextView teks7 = (TextView)rootView.findViewById(R.id.teks7);
		teks7.setText(Html.fromHtml("Musaceae merupakan kelompok pisang-pisangan. Musacea memiliki bentuk semak atau pohon berbatang semu yang terdiri atas pelepah daun; tulang daun menyirip; dan bunga membentuk karangan. Contohnya <i>musa paradisiaca</i> (pisang)."));
		
		TextView teks8 = (TextView)rootView.findViewById(R.id.teks8);
		teks8.setText(Html.fromHtml("Zingiberaceae merupakan kelompok jahe-jahena. Berbentuk semak basah menahun, memiliki batang tegak dengan daun berpelepah yang memeluk batang. Contohnya <i>Zingiber officinales</i> (jahe) dan <i>Alpinia galanga</i> (lengkuas)."));
		
		TextView teks9 = (TextView)rootView.findViewById(R.id.teks9);
		teks9.setText(Html.fromHtml("Cactaceae merupakan kelompok kaktus, memiliki batang yang menyimpan air (sukulen). Daunnya kecil berbentuk sisik (rambut) atau berbentuk duri tempel. Contohnya <i>Opuntia elatior</i>."));
		
		TextView teks10 = (TextView)rootView.findViewById(R.id.teks10);
		teks10.setText(Html.fromHtml("Pandanaceae berbentuk pohon, perdu, atau semak. Daunnya terkumpul rapat dan bertulang daun sejajar. Daun yang rontok meninggalkan bekas berbentuk cincin pada batangnya. Contohnya <i>Pandanus tectorius</i> (pandan)."));
		
		return rootView;
	}


}
