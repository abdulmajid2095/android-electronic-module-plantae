package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Materi6 extends Activity implements OnClickListener{

	Button angio_1,angio_2,angio_3,angio_4,angio_5;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_materi6);
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
		angio_1=(Button)findViewById(R.id.angio__x_ciri);
		angio_1.setOnClickListener(this);
		angio_2=(Button)findViewById(R.id.angio_x_siklus);
		angio_2.setOnClickListener(this);
		angio_3=(Button)findViewById(R.id.angio_x_klasifikasi);
		angio_3.setOnClickListener(this);
		angio_4=(Button)findViewById(R.id.angio_x_peran);
		angio_4.setOnClickListener(this);
		angio_5=(Button)findViewById(R.id.angio_x_uji);
		angio_5.setOnClickListener(this);
		
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		
		case R.id.angio__x_ciri:
			Intent intent1 = new Intent(Materi6.this,Angio_1.class);
			startActivity(intent1);
			break;
		case R.id.angio_x_siklus:
			Intent intent2 = new Intent(Materi6.this,Angio_2.class);
			startActivity(intent2);
			break;
		case R.id.angio_x_klasifikasi:
			Intent intent3 = new Intent(Materi6.this,Angio_3.class);
			startActivity(intent3);
			break;
		case R.id.angio_x_peran:
			Intent intent4 = new Intent(Materi6.this,Angio_4.class);
			startActivity(intent4);
			break;
		case R.id.angio_x_uji:
			Intent intent5 = new Intent(Materi6.this,Angio_5.class);
			startActivity(intent5);
			break;
			
		case R.id.kembali:
			onBackPressed();
			break;

		}
		
	}

}
