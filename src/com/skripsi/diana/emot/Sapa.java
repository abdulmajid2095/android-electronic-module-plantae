package com.skripsi.diana.emot;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class Sapa extends Activity implements OnClickListener{

	TextView sapa;
	Button petunjuk,skip;
	Dataku data;
	MediaPlayer musik2;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_sapa);
		
		sapa=(TextView)findViewById(R.id.Sapa_teks);
		petunjuk=(Button)findViewById(R.id.sapa_petunjuk);
		petunjuk.setOnClickListener(this);
		skip=(Button)findViewById(R.id.sapa_skip);
		skip.setOnClickListener(this);
		
		data = Dataku.findById(Dataku.class, 1L);
		sapa.setText("Hi "+data.nama+", Siap Belajar ?");
		
		musik2 = MediaPlayer.create(this, R.raw.tutorial);
		musik2.setLooping(true);
        musik2.setVolume(1,1);
        musik2.start();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		
		case R.id.sapa_petunjuk:
			musik2.stop();
			musik2.release();
			musik2 = null;
			Intent petunjuk = new Intent(Sapa.this,Petunjuk.class);
			startActivity(petunjuk);
			break;
			
		case R.id.sapa_skip:
			musik2.stop();
			musik2.release();
			musik2 = null;
			Intent skip = new Intent(Sapa.this,MenuUtama.class);
			startActivity(skip);
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
	}


}
