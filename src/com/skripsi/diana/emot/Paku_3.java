package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Paku_3 extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_paku_3);
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
		Button k1 = (Button)findViewById(R.id.paku_klasifikasi_1);
		k1.setOnClickListener(this);
		Button k2 = (Button)findViewById(R.id.paku_klasifikasi_2);
		k2.setOnClickListener(this);
		Button k3 = (Button)findViewById(R.id.paku_klasifikasi_3);
		k3.setOnClickListener(this);
		Button k4 = (Button)findViewById(R.id.paku_klasifikasi_4);
		k4.setOnClickListener(this);
	}
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.kembali:
			onBackPressed();
			break;
			
		case R.id.paku_klasifikasi_1:
			Intent intent1 = new Intent(Paku_3.this,Paku_3a.class);
			startActivity(intent1);
			break;
		case R.id.paku_klasifikasi_2:
			Intent intent2 = new Intent(Paku_3.this,Paku_3b.class);
			startActivity(intent2);
			break;
		case R.id.paku_klasifikasi_3:
			Intent intent3 = new Intent(Paku_3.this,Paku_3c.class);
			startActivity(intent3);
			break;
		case R.id.paku_klasifikasi_4:
			Intent intent4 = new Intent(Paku_3.this,Paku_3d.class);
			startActivity(intent4);
			break;

		}
	}

	

}
