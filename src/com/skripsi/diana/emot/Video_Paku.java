package com.skripsi.diana.emot;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

public class Video_Paku extends Activity {

	VideoView v2;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		setContentView(R.layout.activity_video__paku);
		
		v2 = (VideoView)findViewById(R.id.v2);
		Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.v2);
		v2.setVideoURI(uri);
		v2.setMediaController(new MediaController(this));
		v2.requestFocus();
		v2.start();
	}

	

}
