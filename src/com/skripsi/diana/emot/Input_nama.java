package com.skripsi.diana.emot;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class Input_nama extends Activity implements OnClickListener {

	ImageButton ok;
	EditText nama;
	Dataku data;
	String namaku;
	MediaPlayer musik1;
	Button masuk_nama;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_input_nama);
		
		masuk_nama=(Button)findViewById(R.id.input_nama_masuk);
		masuk_nama.setOnClickListener(this);
		ok = (ImageButton)findViewById(R.id.input_nama_ok);
		ok.setOnClickListener(this);
		nama = (EditText)findViewById(R.id.input_nama_edittext);
		
		String xa = "";
		data = Dataku.findById(Dataku.class, 1L);
		if(data.nama.equals(xa))
		{
			
		}
		else
		{
			masuk_nama.setVisibility(View.VISIBLE);
			masuk_nama.setText("Masuk Sebagai "+data.nama);
		}
		
		musik1 = MediaPlayer.create(this, R.raw.tutorial);
		musik1.setLooping(true);
        musik1.setVolume(1,1);
        musik1.start();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.input_nama_ok:
			namaku=nama.getText().toString();
			if(namaku.equals(""))
			{
				peringatan();
			}
			else
			{
				data.nama=namaku;
				data.level=1;
				data.skor_lumut=0;
				data.skor_paku=0;
				data.skor_gym=0;
				data.skor_ang=0;
				data.save();
				musik1.stop();
				musik1.release();
				musik1 = null;
				Intent intent = new Intent(Input_nama.this,Sapa.class);
				startActivity(intent);
			}
			break;

			case R.id.input_nama_masuk:
				musik1.stop();
				musik1.release();
				musik1 = null;
				Intent intent2 = new Intent(Input_nama.this,MenuUtama.class);
				startActivity(intent2);
			break;
		}
	}
	
	 public void peringatan()
	    {
	    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
			dialogkeluar.setTitle("Error");
			dialogkeluar.setMessage("Silakan Masukkan Nama Anda");
			dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which){
					dialog.dismiss();
				}
			});
			dialogkeluar.show();
	    }

	 @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
	}

}
