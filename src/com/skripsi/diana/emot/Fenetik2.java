package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.text.Html;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Fenetik2 extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_fenetik2);
		
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		
		LinearLayout layout2 = (LinearLayout)findViewById(R.id.zoom2);
		Zoomy.Builder builder2 = new Zoomy.Builder(this)
			.target(layout2);
		builder2.register();
		
		LinearLayout layout3 = (LinearLayout)findViewById(R.id.zoom3);
		Zoomy.Builder builder3 = new Zoomy.Builder(this)
			.target(layout3);
		builder3.register();
		
		LinearLayout layout4 = (LinearLayout)findViewById(R.id.zoom4);
		Zoomy.Builder builder4 = new Zoomy.Builder(this)
			.target(layout4);
		builder4.register();
		
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
		}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.kembali:
			onBackPressed();
			break;

		}
	}

	

}
