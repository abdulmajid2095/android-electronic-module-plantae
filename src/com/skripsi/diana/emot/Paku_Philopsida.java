package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.text.Html;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class Paku_Philopsida extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_paku__philopsida);
		
		TextView teks1 = (TextView)findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("Jenis paku yang termasuk Psilopsida, antara lain <i>Rhynia</i> (paku tidak berdaun) yang telah memfosil. Psilopsida yang masih hidup di bumi yaitu <i>Tmesipteris</i> ditemukan tumbuh di Kepulauan Pasifik. Sementara <i>Psilotum</i> tumbuh di daerah tropis dan sub tropis."));
	}

}
