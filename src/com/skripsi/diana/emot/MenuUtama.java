package com.skripsi.diana.emot;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MenuUtama extends Activity implements OnClickListener{
	
	Button kd,tujuan,materi,ringkasan,uji,glosarium,pustaka,lks;
	Button profil,skor,game;
	MediaPlayer musik4;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_menu_utama);
		
		kd = (Button)findViewById(R.id.menu_utama_kd);
		kd.setOnClickListener(this);
		tujuan =(Button)findViewById(R.id.menu_utama_tujuan);
		tujuan.setOnClickListener(this);
		materi =(Button)findViewById(R.id.menu_utama_materi);
		materi.setOnClickListener(this);
		ringkasan = (Button)findViewById(R.id.menu_utama_ringkasan);
		ringkasan.setOnClickListener(this);
		uji = (Button)findViewById(R.id.menu_utama_uji);
		uji.setOnClickListener(this);
		profil=(Button)findViewById(R.id.menu_utama_profil);
		profil.setOnClickListener(this);
		skor=(Button)findViewById(R.id.menu_utama_skor);
		skor.setOnClickListener(this);
		game=(Button)findViewById(R.id.menu_utama_game);
		game.setOnClickListener(this);
		glosarium=(Button)findViewById(R.id.menu_utama_glosarium);
		glosarium.setOnClickListener(this);
		pustaka=(Button)findViewById(R.id.menu_utama_pustaka);
		pustaka.setOnClickListener(this);
		lks=(Button)findViewById(R.id.menu_utama_lks);
		lks.setOnClickListener(this);
		
		
		musik4 = MediaPlayer.create(this, R.raw.music);
		musik4.setLooping(true);
        musik4.setVolume(1,1);
        musik4.start();
        
        Animation animasi = new TranslateAnimation(0, 500, 0, 0);
		animasi.setDuration(20000);
		animasi.setFillAfter(true);
		final ImageView awan = (ImageView) findViewById(R.id.awanutama);
		awan.startAnimation(animasi);
        
		Animation animasi2 = new TranslateAnimation(0, 700, 0, 0);
		animasi2.setDuration(40000);
		animasi2.setFillAfter(true);
		final ImageView awan2 = (ImageView) findViewById(R.id.awanutama2);
		awan2.startAnimation(animasi2);
		
		animasi1();
		animasi3();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.menu_utama_kd:
			Intent ikd = new Intent(MenuUtama.this,KD.class);
			startActivity(ikd);
			break;

		case R.id.menu_utama_tujuan:
			Intent itujuan = new Intent(MenuUtama.this,Tujuan.class);
			startActivity(itujuan);
			break;
			
		case R.id.menu_utama_materi:
			musik4.stop();
			musik4.release();
			musik4 = null;
			Intent imateri = new Intent(MenuUtama.this,Materi.class);
			startActivity(imateri);
			break;
			
		case R.id.menu_utama_ringkasan:
			Intent iringkasan = new Intent(MenuUtama.this,Ringkasan.class);
			startActivity(iringkasan);
			break;
			
		case R.id.menu_utama_glosarium:
			Intent iglosarium = new Intent(MenuUtama.this,Glosarium.class);
			startActivity(iglosarium);
			break;
			
		case R.id.menu_utama_uji:
			
			break;
			
		case R.id.menu_utama_profil:
			Intent iprofil = new Intent(MenuUtama.this,Profil.class);
			startActivity(iprofil);
			break;
			
		case R.id.menu_utama_skor:
			Intent iskor = new Intent(MenuUtama.this,Skor_Rekap.class);
			startActivity(iskor);
			break;
			
		case R.id.menu_utama_game:
			musik4.stop();
			musik4.release();
			musik4 = null;
			Intent igame = new Intent(MenuUtama.this,Game_Petunjuk.class);
			startActivity(igame);
			break;
		case R.id.menu_utama_pustaka:
			Intent ipus = new Intent(MenuUtama.this,Pustaka.class);
			startActivity(ipus);
			break;
		case R.id.menu_utama_lks:
			musik4.stop();
			musik4.release();
			musik4 = null;
			Intent ilks = new Intent(MenuUtama.this,Analisis.class);
			startActivity(ilks);
			break;
			
		}
	}

	public void keluar()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("KELUAR");
		dialogkeluar.setMessage("Ingin keluar dari aplikasi ?");
		dialogkeluar.setPositiveButton ("Tidak", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.setNegativeButton("Ya", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which )
			{
				musik4.release();
				musik4 = null;
			    Intent keluar = new Intent(MenuUtama.this, MainActivity.class);
				startActivity(keluar);
				Intent intent = new Intent(Intent.ACTION_MAIN);
			    intent.addCategory(Intent.CATEGORY_HOME);
			    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    startActivity(intent);
			    android.os.Process.killProcess(android.os.Process.myPid());
				finish();
				System.exit(1);
			}
		});
		dialogkeluar.show();
    }
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		keluar();
	}
	
	public void animasi1()
	{
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Animation animasi = new TranslateAnimation(500, 0, 0, 0);
				animasi.setDuration(20000);
				animasi.setFillAfter(true);
				final ImageView awan = (ImageView) findViewById(R.id.awanutama);
				awan.startAnimation(animasi);
				animasi2();
			}
		},20000);	
	}
	
	public void animasi2()
	{
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Animation animasi = new TranslateAnimation(0, 500, 0, 0);
				animasi.setDuration(20000);
				animasi.setFillAfter(true);
				final ImageView awan = (ImageView) findViewById(R.id.awanutama);
				awan.startAnimation(animasi);
				animasi1();
			}
		},20000);	
	}
	
	public void animasi3()
	{	
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Animation animasi2 = new TranslateAnimation(700, 0, 0, 0);
				animasi2.setDuration(40000);
				animasi2.setFillAfter(true);
				final ImageView awan2 = (ImageView) findViewById(R.id.awanutama2);
				awan2.startAnimation(animasi2);
				animasi4();
			}
		},40000);
	}
	
	public void animasi4()
	{	
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Animation animasi2 = new TranslateAnimation(0, 700, 0, 0);
				animasi2.setDuration(40000);
				animasi2.setFillAfter(true);
				final ImageView awan2 = (ImageView) findViewById(R.id.awanutama2);
				awan2.startAnimation(animasi2);
				animasi3();
			}
		},40000);
	}

}
