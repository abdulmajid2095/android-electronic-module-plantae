package com.skripsi.diana.emot;

import java.util.ArrayList;
import java.util.List;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class Spermatophyta_Uji extends Activity implements OnClickListener{

	Button jawab;
	LinearLayout utama;
	String[] jawaban={"a. Memiliki sistem vaskule","b. Dominan dengan fase sporofit","c. Merupakan tumbuhan bertalus","d. akar serabut/tunggang","e. memiliki strobilus atau bunga",
					"a. Kumpulan spora","b. Kumpulan sporofil","c. Kumpulan sporangiofor","d. Kumpulan dari karpel (daun biji)","e. Kumpulan endospermae yang menebal",
					"a. Pinus mercusii","b. Oryza sativa","c. Solanum lypersicum","d. Gnetum gnemon","e. Ginkgo biloba",
					"a. Serbuk sari","b. Kepala putik","c. Kepala sari","d. Dasar putik","e. Bakal biji",
					"a. Akan membelah dua","b. Tidak akan membelah dua","c. Melebur dengan inti kandung lembaga sekunder","d. Melebur dengan sinergid","e. Melebur dengan sel telur",
					"a. Rosales","b. Piperales","c. Malvales","d. Fabales","e. Casuarinales",
					"a. Satu sel megaspora yang hidup","b. Dua sel megaspora yang hidup","c. Tiga sel megaspora yang hidup","d. Empat sel megaspora yang hidup","e. Lima sel megaspora yang hidup",
					"a. Pandanus tectorius","b. Bouginvillea spectabilis","c. Cocos nucifera","d. Michelia campaka","e. Zingiber officinale",
					"a. Gymnospermae","b. Lycopsida","c. Hepaticopsida","d. Magnoliopsida","e. Liliopsida",
					"a. Pembuluh angkut dari bawah ke atas","b. Alat reproduksi generatif","c. Bakal biji","d. Pembuahan","e. Jumlah zigot",
					"a. Inti sel sperma haploid membuahi ovum haploid","b. Inti sel sperma haploid membuahi dua sinergid haploid","c. haploid membuahi inti kandung lembaga sekuder diploid","d. Inti sel sperma haploid membuahi tiga sel antipoda","e. Inti sel sperma haploid membuahi dua ovum",
					"a. Cycadinae, Coniferae, lycopsida, Gnetinae","b. Cycadinae. Coniferae, Gnetinae, Ginkgoinae","c. Coniferae, Gnetinae, Ginkgoinae, Liliopsida","d. Ginkgoinae, Lycopsida, Hepaticopsida, Coniferae","e. Liliopsida, Cycadinae, Gnetinae, Coniferae",
					"a. Ginkgo biloba","b. Tectona grandis","c. Zamia floridana","d. Gnetum gnemon","e. Cycas rumpii",
					"a. Menghasilkan dua sperma dan membuahi dua ovum secara bersama","b. Menghasilkan dua sperma, sperma I membuahi ovum sperma II membuahi sel sinergid","c. Menghasilkan dua sperma, sperma I membuahi ovum sperma II membuahi sel antipoda","d. Menghasilkan dua sperma, sperma I membuahi ovum sperma II membuahi Inti kandung Lembaga sekunder","e. Menghasilkan dua sperma, dan keduanya membuahi satu ovum",
					"a. Cycadinae","b. Coniferae","c. Gnetinae","d. Ginkgoinae","e. Zingiberales",
};
	
	
	
	String[] soal={"1.	Salah satu ciri tumbuhan berbiji, kecuali �",
					"2.	Stobilus merupakan istilah dari tumbuhan berbiji yang berasal dari �",
					"3.	Tanaman berikut yang dimanfaatkan sebagai bahan kosmetik untuk anti penuaan adalah �",
					"4.	Pada tumbuhan berbiji tertutup, mikrospora terdapat pada �",
					"5. Pembuahan pada mangga didahului dengan penyerbukan. Dalam serbuk sari terbentuk inti vegetatif dan inti generatif. Saat perjalanan menuju ke bakal buah, inti generatif �",
					"6. Ditemukan tumbuhan dengan ciri-ciri berikut:\n1) Berbentuk perdu\n2) Tulang daun melengkung\n3)	Daun berbentuk seperti hati\n4) Batang membelit\n5) Memiliki bau aromatik\nBerdasarkan pengamatan tumbuhan diatas, tumuhan tersebut dapat digolongkan kedalam � ",
					"7. Pada bakal biji terdapat sel induk megaspora yang membelah secara meiosis menghasilkan �",
					"8. Manfaat tumbuhan Spermatophyta yang digunakan sebagai obat-obatan yaitu ..",
					"9. Seorang siswa mengamati tumbuhan dengan ciri-ciri beikut:",
					"10. Berikut ini merupakan perbedaan Gymnospermae dan Angiospermae, kecuali �",
					"11. Pembentukan endospermae terjadi ketika �",
					"12. Gymnospermae dibagi menjadi empat kelas yaitu �",
					"13. Salah satu manfaat tumbuhan Spermatophyta yang digunakan untuk bahan industri kayu adalah �",
					"14. Pembuahan pada Angiospermae disebut pembuahan ganda karena �",
					"15. Dalam perjalanan pulang kerumah seorang siswa menemukan tumbuhan dengan ciri-ciri:"
	};
	
	String[] kunci={"c","b","e","c","a",
					"b","a","e","e","e",
					"c","b","b","d","c"};
	
	
	int benar=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		
		utama = (LinearLayout)findViewById(R.id.layout_uji);
		
		
		jawab = (Button)findViewById(R.id.uji_tombol_jawab);
		jawab.setOnClickListener(this);
		
		looping();
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.uji_tombol_jawab:
			int skorku = (benar+5)*5;
			Dataku data = Dataku.findById(Dataku.class, 1L);
			/*if(skorku>data.skor_sper)
			{
				data.skor_sper = skorku;
				data.save();
			}*/
			
			Intent intent = new Intent(getApplicationContext(),Skor.class);
			intent.putExtra("pilihan", "sper");
			intent.putExtra("skor", ""+skorku);
			startActivity(intent);
			break;
			
		}
	}
	
	public void looping()
	{
		for(int l=0;l<15;l++)
		{	
		
		//Soal
		TextView namawisata = new TextView(getApplicationContext());
		namawisata.setText(""+soal[l]);
		namawisata.setTextSize(15);
		LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT );
		namawisata.setLayoutParams(params1);
		
		final RadioGroup rg = new RadioGroup(getApplicationContext());
		rg.setId(l);
		int n1=l+1;
		int n2=n1*5;
		int n3=n2-5;
		
        for(int i=0;i<5;i++){
            RadioButton rb=new RadioButton(getApplicationContext()); // dynamically creating RadioButton and adding to RadioGroup.
            //rb.setId(R.id.);
            rb.setText(""+jawaban[n3+i]);
            rg.addView(rb);
        }
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				int childCount = group.getChildCount();
                for (int x = 0; x < childCount; x++) {
                   RadioButton btn = (RadioButton) group.getChildAt(x);
                   if (btn.getId() == checkedId) {
                	   
                	   String ans = btn.getText().toString();
                	   String ans2 = ans.subSequence(0, 1).toString();
                	   int d = rg.getId();
                	   //Toast.makeText(getActivity(), ""+d, Toast.LENGTH_LONG).show();
                	   if(ans2.equals(kunci[d]))
                	   {
                		   benar=benar+1;
                	   }

                   }
                }
			}
		});
        
        LinearLayout lay1 = new LinearLayout(getApplicationContext());
        lay1.setBackgroundColor(Color.rgb(65, 148, 92));
		LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,2);
		params2.bottomMargin=10;
		params2.topMargin=10;
		lay1.setLayoutParams(params2);
       
		
		utama.addView(namawisata);
		utama.addView(rg);
		utama.addView(lay1);
		
		}
	}


}
