package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Angio_3b extends Activity implements OnClickListener{

	Button diko_1,diko_2;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_angio_3b);
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		diko_1=(Button)findViewById(R.id.diko__x_ciri);
		diko_1.setOnClickListener(this);
		diko_2=(Button)findViewById(R.id.diko_x_klasifikasi);
		diko_2.setOnClickListener(this);
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		
		case R.id.diko__x_ciri:
			Intent intent1 = new Intent(Angio_3b.this,Diko_1.class);
			startActivity(intent1);
			break;
		case R.id.diko_x_klasifikasi:
			Intent intent3 = new Intent(Angio_3b.this,Diko_2.class);
			startActivity(intent3);
			break;
		
			
		case R.id.kembali:
			onBackPressed();
			break;

		}
		
	}

}
