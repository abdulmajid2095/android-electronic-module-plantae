package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

public class Materi_Paku extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_materi__paku);
		
		ViewPager pager = (ViewPager) findViewById(R.id.pager_paku);
        FragmentManager fm = getSupportFragmentManager();
        Adapter_paku_tab pagerAdapter = new Adapter_paku_tab(fm);
        pager.setAdapter(pagerAdapter);
	}


}
