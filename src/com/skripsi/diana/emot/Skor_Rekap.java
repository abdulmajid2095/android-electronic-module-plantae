package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class Skor_Rekap extends Activity implements OnClickListener {

	TextView nama,skor_lumut,skor_paku,skor_gym,skor_ang;
	Dataku data;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_skor__rekap);
		
		data = Dataku.findById(Dataku.class, 1L);
		nama=(TextView)findViewById(R.id.rekap_nama);
		nama.setText(""+data.nama);
		skor_lumut=(TextView)findViewById(R.id.rekap_lumut);
		skor_lumut.setText(""+data.skor_lumut);
		skor_paku=(TextView)findViewById(R.id.rekap_paku);
		skor_paku.setText(""+data.skor_paku);
		skor_gym=(TextView)findViewById(R.id.rekap_gym);
		skor_gym.setText(""+data.skor_gym);
		skor_ang=(TextView)findViewById(R.id.rekap_ang);
		skor_ang.setText(""+data.skor_ang);
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.kembali:
			onBackPressed();
			break;
		}
	}

	

}
