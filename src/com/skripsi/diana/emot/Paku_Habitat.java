package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Paku_Habitat extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_paku__habitat, container, false);
		
		TextView teks1 = (TextView)rootView.findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("Di tanah : <i>Adhiantum cuneatum</i> (suplir) dan <i>Alsophila glauca</i> (paku tiang)."));
		
		TextView teks2 = (TextView)rootView.findViewById(R.id.teks2);
		teks2.setText(Html.fromHtml("Di tempat yang lembab (higrofit): <i>Marsilea sp.</i>"));
		
		TextView teks3 = (TextView)rootView.findViewById(R.id.teks3);
		teks3.setText(Html.fromHtml("Di air (hidrofit): <i>Azzola pinnata</i> dan <i>Salvinia natans</i>."));
		
		TextView teks4 = (TextView)rootView.findViewById(R.id.teks4);
		teks4.setText(Html.fromHtml("Menempel (epifit) di kulit pohon:  <i>Platycerium bifurcatum</i> (paku tanduk rusa) dan <i>Asplenium nidus</i> (paku sarang burung)"));
		
		return rootView;
	}


}
