package com.skripsi.diana.emot;



import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class Adapter_paku_tab extends FragmentPagerAdapter {

	public Adapter_paku_tab(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int index) {
		switch (index) {
        case 0:
            // Top Rated fragment activity
            return new Paku_Pengertian();
        case 1:
            // Games fragment activity
        	return new Paku_Habitat();
        case 2:
            // Games fragment activity
        	return new Paku_Morfologi();
        case 3:
            // Games fragment activity
        	return new Paku_Reproduksi();
        case 4:
            // Games fragment activity
        	return new Paku_Metagenesis();
        case 5:
            // Games fragment activity
        	return new Paku_Klasifikasi();
        case 6:
            // Games fragment activity
        	return new Paku_Peranan();
        case 7:
            // Games fragment activity
        	return new Paku_Video();
        case 8:
            // Games fragment activity
        	return new Paku_Uji();
        }
		
 
        return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 9;
	}
	
	@Override
    public CharSequence getPageTitle(int position) {
        //return "Page #" + ( position + 1 );
    	switch (position) {
        case 0:
            // Top Rated fragment activity
        	return "Tentang Tumbuhan Paku";
        case 1:
            // Games fragment activity
        	return "Cara Hidup dan Habitat";
        case 2:
            // Games fragment activity
        	return "Ciri Morfologi";
        case 3:
            // Games fragment activity
        	return "Reproduksi";
        case 4:
            // Games fragment activity
        	return "Siklus Hidup";
        case 5:
            // Games fragment activity
        	return "Klasifikasi";
        case 6:
            // Games fragment activity
        	return "Peranan Tumbuhan Paku";
        case 7:
            // Games fragment activity
        	return "Video";
        case 8:
            // Games fragment activity
        	return "Uji Kompetensi";
        }
 
        return null;
    }

}
