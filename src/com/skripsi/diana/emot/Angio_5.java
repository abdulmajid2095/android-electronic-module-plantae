package com.skripsi.diana.emot;

import java.util.ArrayList;
import java.util.List;

import com.ablanco.zoomy.Zoomy;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class Angio_5 extends Activity implements OnClickListener{

	Button jawab;
	LinearLayout utama;
	String[] jawaban={"a. sel antipoda","b. inti sinergig","c. inti lembar primer","d. inti kandung lembaga primer","e. inti kandung lembaga sekunder",
			"a. poaceae","b. malvaceace","c. papilionaceae","d. euporbiaceae","e. zingiberaceae",
			"a. susunan akarnya","b. morfologi bunganya","c. tipe perkecambahannya","d. susunan anatomi batang","e. sifat haploid sel kelaminnya",
			"a. kulit buah","b. endosperm","c. bakal buah","d. kulit biji tebal","e. berada dalam strobilus",
			"a. Gymnospermae dan Dicotyledonae","b. Pteridophyta dan Bryophyta","c. Magnoliopsida dan Liliopsida","d. Magnoliopsida dan Pteridophyta","e. Angiospermae dan Dicotyledonae",
			"a. 1","b. 4","c. 2","d. 5","e. 3",
			"a. Endosperma (2n)","b. Endosperma (3n)","c. Endosperma (n)","d. Zigot (n)","e. Zigit (2n)",
			"a. 1 dan 2","b. 2 dan 5","c. 1 dan 3","d. 3 dan 5","e. 2 dan 3",
			"a. Casuarinaceae","b. Papilionaceae","c. Malvaceae","d. Minosaceae","e. Poaceae",
			"a. Menghasilkan zigot (2n) dan endosperma (2n)","b. Menhasilkan zigot (2n) dan endosperma (3n)","c. Menghasilkan zigot (n) dan endosperma (3n)","d. Menghasilkan zigot (2n) dan endosperma (n)","e. Menghasilkan zigot (n) dan endospermae (2n)",
			"a. Bunga pada sporofil (2n) memiliki kepala sari yang di dalamnya terdapat sel induk mikrospora (2n)","b. Sel induk mikrospora membelah secara meiosis menghasilkan mikrospora haploid (n)","c. Mikrospora mengalami pembelahan mitosis menghasilkan gametofit jantan berupa butir serbuk yang haploid (n)","d. Pada bakal biji, sel induk megaspora (2n) membelah secara mitosis menghasilkan empat sel megaspora (n)","e. Megaspora yang hidup membentuk gametofit betina",
			"a. Lilium regale","b. Oryza sativa","c. Zea mays","d. Solanum tubermosum","e. Zingiber officinale",
			"a. Apinia galanga","b. Cocos nucifera","c. Pandanus tectorius","d. Elaeis guineensis","e. Ananas comusus",
			"a. Rosales","b. Piperales","c. Malvales","d. Fabales","e. casuarinales",
			"a. Zingiber officinale","b. Bouginvillea spectabilis","c. Cocos nucifera","d. Michelia campaka","e. Allamanda cathartica",
	};
	
	
	
	String[] soal={"1.	Pada angiospermae terjadi pertumbuhan ganda dengan adanya peleburan antara inti generatif dengan sel telur dan inti generatif dengan �",
					"2.	Padi, jagung, rumput, alang-alang termasuk kedalam famili �",
					"3.	Berikut ini yang bukan merupakan ciri-ciri perbedaan struktur tumbuhan monokotil dan dikotil adalah �",
					"4.	Angiospermae sering disebut sebagai tumbuhan berbiji tertutup. Disebut berbiji tertutup karena bakal bijinya ditutupi oleh �",
					"5. Angiospermae digolongkan menjadi dua kelas, yaitu �",
					"6. Perhatikan ciri tumbuhan berikut!\n1) akar tunggang tulang, daun menjari\n2) tidak berbunga, terdapat berkas pengangkut\n3) akar serabut, tulang daun sejajar\n4) mempunyai spora pada daun\n5) tidak dapat dibedakan antara batang, daun, dan akar\nBerdasarkan ciri di atas, yang termasuk tumbuhan monokotil yaitu �",
					"7. Pada Angispermae, inti sperma II akan membuahi inti kandung lembaga sekuder dan menghasilkan �",
					"8. Perhatikan gambar proses fertlisasi dan organ reproduksi pada tumbuhan berbiji berikut. Bagian terjadinya peleburan inti yang akan menghasilkan zigot yaitu nomor �",
					"9. Berikut ini yang merupakan famili dari kelas monokotil yaitu famili �",
					"10. Angiospermae disebut pembuahan ganda dikarenakan �",
					"11. Pernyataan beriku yang salah tentang siklus hidup Angiospermae adalah �",
					"12. Berikut ini yang tidak termasuk contoh tumbuhan monokotil adalah �",
					"13. Tumbuhan di bawah ini yang termasuk dalam family Arecaceae adalah �",
					"14. Ditemukan tubuhan dengan ciri-ciri berikut:\n1) Berbentuk perdu\n2) Tulang daun melengkung\n3) Daun berbentuk seperti hati\n4) Batang membelit\n5) Memiliki bau aromatik\nBerdasarkan pengamatan tumbuhan di atas, tumbuhan tersebut dapat digolongkan ke dalam �",
					"15. Tanaman Angiopsermae yang digunakan sebagai obat-obatan dan sebagai bahan masakan yaitu �"
	};
	
	String[] kunci={"e","a","e","c","c",
					"e","b","c","e","b",
					"d","d","b","b","a"};
	
	
	int benar=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_lumut_5);
		
		utama = (LinearLayout)findViewById(R.id.layout_uji);
		jawab = (Button)findViewById(R.id.uji_tombol_jawab);
		jawab.setOnClickListener(this);
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		looping();
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.kembali:
			onBackPressed();
			break;
		case R.id.uji_tombol_jawab:
			int skorku = (benar*2/3)*10;
			Dataku data = Dataku.findById(Dataku.class, 1L);
			if(skorku>data.skor_ang)
			{
				data.skor_ang = skorku;
				data.save();
			}
			
			Intent intent = new Intent(Angio_5.this,Skor.class);
			intent.putExtra("pilihan", "Angiospermae");
			intent.putExtra("skor", ""+skorku);
			startActivity(intent);
			break;
			
		}
	}
	
	public void looping()
	{
		for(int l=0;l<15;l++)
		{	
			
		
		//Soal
		TextView namawisata = new TextView(this);
		namawisata.setText(""+soal[l]);
		namawisata.setTextSize(15);
		LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT );
		namawisata.setLayoutParams(params1);
		
		
		
		final RadioGroup rg = new RadioGroup(this);
		rg.setId(l);
		int n1=l+1;
		int n2=n1*5;
		int n3=n2-5;
		
        for(int i=0;i<5;i++){
            RadioButton rb=new RadioButton(this); // dynamically creating RadioButton and adding to RadioGroup.
            //rb.setId(R.id.);
            rb.setText(""+jawaban[n3+i]);
            rg.addView(rb);
        }
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				int childCount = group.getChildCount();
                for (int x = 0; x < childCount; x++) {
                   RadioButton btn = (RadioButton) group.getChildAt(x);
                   if (btn.getId() == checkedId) {
                	   
                	   String ans = btn.getText().toString();
                	   String ans2 = ans.subSequence(0, 1).toString();
                	   int d = rg.getId();
                	   //Toast.makeText(getActivity(), ""+d, Toast.LENGTH_LONG).show();
                	   if(ans2.equals(kunci[d]))
                	   {
                		   benar=benar+1;
                	   }

                   }
                }
			}
		});
        
        LinearLayout lay1 = new LinearLayout(this);
        lay1.setBackgroundColor(Color.rgb(65, 148, 92));
		LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,2);
		params2.bottomMargin=10;
		params2.topMargin=10;
		lay1.setLayoutParams(params2);
       
		
		utama.addView(namawisata);
		if(l==7)
		{
			LinearLayout.LayoutParams GambarParams = new LinearLayout.LayoutParams(500, 400);
			ImageView gambar = new ImageView(this);
	    	GambarParams.gravity = Gravity.CENTER;
			gambar.setScaleType(ScaleType.CENTER_INSIDE);
			gambar.setLayoutParams(GambarParams);
			gambar.setImageResource(R.drawable.soal8);
			utama.addView(gambar);
		}
		utama.addView(rg);
		utama.addView(lay1);
		
		}
	}


}
