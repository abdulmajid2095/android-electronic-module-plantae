package com.skripsi.diana.emot;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

public class Video_Lumut extends Activity{

	VideoView v1;
	Button play,stop,back;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		setContentView(R.layout.activity_video__lumut);
		
		v1 = (VideoView)findViewById(R.id.v1);
		Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.v1);
		v1.setVideoURI(uri);
		v1.setMediaController(new MediaController(this));
		v1.requestFocus();
		v1.start();
		
		

	}

	

}
