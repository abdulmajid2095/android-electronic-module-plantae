package com.skripsi.diana.emot;

import java.util.ArrayList;
import java.util.List;

import com.ablanco.zoomy.Zoomy;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class Gymno_5 extends Activity implements OnClickListener{

	Button jawab;
	LinearLayout utama;
	String[] jawaban={"a. Pakis haji, pinus, damar, melinjo","b. Cemara, pinus, damar, pinang","c. Alang-alang, pakis haji, damar, melinjo","d. Pinus, pinang, pakis haji, damar, melinjo","e. Damar, alang-alang, cemara, pakis haji",
			"a. Dioon edule","b. Ginkyo biloba","c. Pinus merkusii","d. Gnetum gnemon","e. Cycas rumphii",
			"a. Cycadinae, Coniferae, Lycopsida, Gnetinae","b. Cycadinae Coniferae, Gnetinae, Ginkgoinae","c. Coniferae, Gnetinae, Ginkgoinae, Liliopsida","d. Ginkgoinae, Lycopsida, Hepaticopsida, Coniferae","e. Liliopsida, Cycadinae, Gnetinae, Coniferae",
			"a. Ginkgoinae","b. Pinaceae","c. Gnetinae","d. Cycadinae","e. Coniferae",
			"a. Pohon pinus (2n) membentuk stobilus jantan dan strobilus betina","b. Strobilus jantan menghasilkan gametofit jantan berupa butir serbuk sari (n)","c. Penyerbukan terjadi bila serbuk sari jatuh pada strobilus betina melalui mikrofil","d. Di dalam strobilus betia terjadi pembelahan meiosis dihasilkan dua sel hapoid (n) dan tumbuh menjadi megaspora","e. Megaspora membelah secara mitosis berulang-ulang dan tumbuh menjadi jaringa gametofit betina",			
			"a. Berkas pengangkut","b. Polen","c. Karpela","d. Ovarium","e. Ovum ",
			"a. Serbuk sari","b. Kepala putik","c. Kepala sari","d. Dasar putik","e. Bakal biji",
			"a. Biji","b. Trakeid","c. Buah","d. Pebuluh tapis","e. Serbuk sari",
			"a. Pohonya bertunas pendek","b. Tulang daun bercabang menggarpu","c. Batangnya terdiri dari hipokotil yang menebal","d. Bijinya mepunyai kulit luar yang berdaging dan kulit dalam yang keras","e. Daunnya bertangkai panjang berbentuk pasak atau kipas",
			"a. Bakal biji","b. integumen","c. Tabung serbuk sari","d. Kulit biji","e. mikropil",
			"a. Sel steril dan sel ovum","b. Sel spermatogen dan sel endospermae","c. Sel steril dan sel spermatogen","d. Dua sel ovum","e. Dua sel spermatogen",
			"a. Cycadinae","b. Ginkgoinae","c. Gnetinae","d. Lycopsida","e. Coniferae",
			"a. Gnetum gnemon","b. Opuntia elatior","c. Musa paradisiaca","d. Lilium regium","e. Piper betle",
			"a. Berakar tunggang","b. Memiliki strobillus","c. Memiliki buah","d. Pembuahan tunggal","e. Belum memiliki bunga sebenarnya",
			"a. Macam-macam jaringan pembuluh","b. Bunganya","c. Buahnya","d. Cara pembuahannya","e. Ikatan pembuluh yang terbuka"
};
	
	
	
	String[] soal={"1.	Dari deretan tumbuhan berikut yang semuanya termasuk ke dalam Gymnospermae adalah ...",
					"2.	Tumbuhan Gymnospermae yang berperan untuk bahan makanan yaitu...",
					"3.	Gymnospermae dibagi menjadi empat kelas yaitu ...",
					"4.	Berikut ini tidak termasuk contoh kelas dari subdivisi Gymnospermae adalah ...",
					"5. Peryataan siklus hidup Gymnospermae yang benar adalah ...",
					"6. Tumbuhan Gymnopsermae dan angiopsermae memiliki bagian-bagian berikut, kecuali ...",
					"7. Pada tumbuhan kormofita berbiji tertutup, mikrospora terdapat dalam ...",
					"8. Angiospermae berbeda dari Gymnospermae karena pada tumbuhan berbiji terbuka (Gymnospermae) tidak terdapat ...",
					"9. Gymnospermae dibagi menjadi beberrapa divisi. Divisi Gingkopyta memunyai ciri-ciri sebagai berikut, kecuali ...",
					"10. Pada Gymnospermae, penyerbukan terjadi bila serbuk sari jatuh pada strobilus betina malalui ...",
					"11. Dalam serbuk sari terdapat sel generati yang membelah menjadi dua sel, yaitu ...",
					"12. Ciri tumbuhan, berbentuk seperti jamur atau sisik, tampak selalu berwarna hijau (evergreen), konus jantan berukuran lebih kecil dibanding konus betina. Merupakan ciri tumbuhan",
					"13. Tumbuhan berikut yang termasuk ke dalam tumbuha biji terbuka ...",
					"14. Berikut ini yang merupakan ciri tumbuhan Gymnospermae, kecuali ...",
					"15. Persamaan Dycotiledoneae dengan Gymnospermae adalah ..."
	};
	
	String[] kunci={"a","d","b","b","d","d","c","b","a","e","c","e","a","a","e"};
	
	
	int benar=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_gymno_5);
		
		utama = (LinearLayout)findViewById(R.id.layout_uji);
		
		
		jawab = (Button)findViewById(R.id.uji_tombol_jawab);
		jawab.setOnClickListener(this);
		
		looping();
	}
	
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.uji_tombol_jawab:
			int skorku = (benar*2/3)*10;
			Dataku data = Dataku.findById(Dataku.class, 1L);
			if(skorku>data.skor_gym)
			{
				data.skor_gym = skorku;
				data.save();
			}
			
			Intent intent = new Intent(Gymno_5.this,Skor.class);
			intent.putExtra("pilihan", "gymno");
			intent.putExtra("skor", ""+skorku);
			startActivity(intent);
			break;
			
		}
	}
	
	public void looping()
	{
		for(int l=0;l<15;l++)
		{	
		
		//Soal
		TextView namawisata = new TextView(this);
		namawisata.setText(""+soal[l]);
		namawisata.setTextSize(15);
		LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT );
		namawisata.setLayoutParams(params1);
		
		final RadioGroup rg = new RadioGroup(this);
		rg.setId(l);
		int n1=l+1;
		int n2=n1*5;
		int n3=n2-5;
		
        for(int i=0;i<5;i++){
            RadioButton rb=new RadioButton(this); // dynamically creating RadioButton and adding to RadioGroup.
            //rb.setId(R.id.);
            rb.setText(""+jawaban[n3+i]);
            rg.addView(rb);
        }
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				int childCount = group.getChildCount();
                for (int x = 0; x < childCount; x++) {
                   RadioButton btn = (RadioButton) group.getChildAt(x);
                   if (btn.getId() == checkedId) {
                	   
                	   String ans = btn.getText().toString();
                	   String ans2 = ans.subSequence(0, 1).toString();
                	   int d = rg.getId();
                	   //Toast.makeText(getActivity(), ""+d, Toast.LENGTH_LONG).show();
                	   if(ans2.equals(kunci[d]))
                	   {
                		   benar=benar+1;
                	   }

                   }
                }
			}
		});
        
        LinearLayout lay1 = new LinearLayout(this);
        lay1.setBackgroundColor(Color.rgb(65, 148, 92));
		LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,2);
		params2.bottomMargin=10;
		params2.topMargin=10;
		lay1.setLayoutParams(params2);
       
		
		utama.addView(namawisata);
		utama.addView(rg);
		utama.addView(lay1);
		
		}
	}


}
