package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.text.Html;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Gymno_4 extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_gymno_4);
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
		TextView teks1 = (TextView)findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("Bahan industri kertas: <i>Podocarpus sp, Pinus mercusii, Sequoia sp,</i> dan <i>Agathis sp</i>"));
		TextView teks2 = (TextView)findViewById(R.id.teks2);
		teks2.setText(Html.fromHtml("Obat-obatan: <i>Ginkgo biloba</i> dan <i>Pinus</i> (getahnya untuk obat luka)"));
		TextView teks3 = (TextView)findViewById(R.id.teks3);
		teks3.setText(Html.fromHtml("Kosmetik: <i>Ginkgo biloba</i>, sebagai agen anti penuaan"));
		TextView teks4 = (TextView)findViewById(R.id.teks4);
		teks4.setText(Html.fromHtml("Bahan makanan: <i>Gnetum gnemon</i> (daun untuk sayuran dan bijin untuk emping)"));
		TextView teks5 = (TextView)findViewById(R.id.teks5);
		teks5.setText(Html.fromHtml("Tanaman hias: <i>Cycas, dioon edule,</i> dan <i>Cupressus</i>"));
		TextView teks6 = (TextView)findViewById(R.id.teks6);
		teks6.setText(Html.fromHtml("Bahan kayu bangunan: <i>Podocarpus, Sequoia</i> dan <i>Adathis</i>"));
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.kembali:
			onBackPressed();
			break;

		}
	}

	

}
