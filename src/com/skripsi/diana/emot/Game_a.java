package com.skripsi.diana.emot;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.text.Layout;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class Game_a extends Activity implements OnClickListener{

	LinearLayout utama;
	int level;
	int 	mulai;
	Dataku data;
	MediaPlayer musik6;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_game_a);
		
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
		data = Dataku.findById(Dataku.class, 1L);
		level=data.level;
		//level = 15;
		utama = (LinearLayout)findViewById(R.id.game_layout);
		mulai = 15-level;
		looping();
		if(mulai==0)
		{
			
		}
		else
		{
			looping2();
		}
		
		musik6 = MediaPlayer.create(this, R.raw.chiptune2);
		musik6.setLooping(true);
        musik6.setVolume(1,1);
        musik6.start();
	}

	public void looping()
	{
		for(int l=1;l<=level;l++)
		{	
		
		//Soal
		final Button tombol_level = new Button(this);
		tombol_level.setText("Level\n"+l);
		tombol_level.setBackgroundResource(R.drawable.ga);
		tombol_level.setTextSize(25);
		tombol_level.setId(l);
		tombol_level.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				musik6.stop();
				musik6.release();
				musik6 = null;
				Intent intent = new Intent(Game_a.this,Game_b.class);
				intent.putExtra("level", ""+tombol_level.getId());
				startActivity(intent);
			}
		});
		
		LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		params1.setMargins(10, 10, 10, 10);
		tombol_level.setLayoutParams(params1);
		
		
		utama.addView(tombol_level);
		
		}
	}
	
	public void looping2()
	{
		for(int k=1;k<=mulai;k++)
		{	
		//Soal
		int zz = level+k;
		Button tombol_level = new Button(this);
		tombol_level.setText("Level\n"+zz);
		tombol_level.setBackgroundResource(R.drawable.ga3);
		tombol_level.setTextSize(25);
		LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		params1.setMargins(10, 10, 10, 10);
		tombol_level.setLayoutParams(params1);
		
		utama.addView(tombol_level);
		
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		musik6.stop();
		musik6.release();
		musik6 = null;
		Intent intent = new Intent(Game_a.this,MenuUtama.class);
		startActivity(intent);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case R.id.kembali:
				onBackPressed();
				break;
		}
	}
	
}
