package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.text.Html;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class Lumut_Hati extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_lumut__hati);
		
		TextView teks1 = (TextView)findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("Kapsul spora Marchantiales dapat menghasilkan beratus ribu spora. Jika jatuh di tempat yang sesuai, spora ini akan berkecambah membentuk protonema dan seterusnya. Contoh lumut yang termasuk suku Marchantiales adalah <i>Marchantia polymorpha, M. geminata, dan Reboulia hemishaerica</i>, sedangkan yang termasuk suku Ricciaceae adalah <i>Ricca fluitans, R.nutans, dan R.  trichocarpa</i>"));
	}

	
}
