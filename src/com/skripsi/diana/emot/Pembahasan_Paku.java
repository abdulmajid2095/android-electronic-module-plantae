package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Pembahasan_Paku extends Activity implements OnClickListener{

	Button menu;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_pembahasan__paku);
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		menu = (Button)findViewById(R.id.pembahasan_menu);
		menu.setOnClickListener(this);
		
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
	}
	
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.pembahasan_menu:
			Intent intent = new Intent(Pembahasan_Paku.this,MenuUtama.class);
			startActivity(intent);
			break;
		case R.id.kembali:
			onBackPressed();
			break;
		}
	}


}
