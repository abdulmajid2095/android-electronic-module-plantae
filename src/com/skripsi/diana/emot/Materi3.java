package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Materi3 extends Activity implements OnClickListener{

	Button paku_1,paku_2,paku_3,paku_4,paku_5;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_materi3);
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
		paku_1=(Button)findViewById(R.id.paku__x_ciri);
		paku_1.setOnClickListener(this);
		paku_2=(Button)findViewById(R.id.paku_x_siklus);
		paku_2.setOnClickListener(this);
		paku_3=(Button)findViewById(R.id.paku_x_klasifikasi);
		paku_3.setOnClickListener(this);
		paku_4=(Button)findViewById(R.id.paku_x_peran);
		paku_4.setOnClickListener(this);
		paku_5=(Button)findViewById(R.id.paku_x_uji);
		paku_5.setOnClickListener(this);
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		
		case R.id.paku__x_ciri:
			Intent intent1 = new Intent(Materi3.this,Paku_1.class);
			startActivity(intent1);
			break;
		case R.id.paku_x_siklus:
			Intent intent2 = new Intent(Materi3.this,Paku_2.class);
			startActivity(intent2);
			break;
		case R.id.paku_x_klasifikasi:
			Intent intent3 = new Intent(Materi3.this,Paku_3.class);
			startActivity(intent3);
			break;
		case R.id.paku_x_peran:
			Intent intent4 = new Intent(Materi3.this,Paku_4.class);
			startActivity(intent4);
			break;
		case R.id.paku_x_uji:
			Intent intent5 = new Intent(Materi3.this,Paku_5.class);
			startActivity(intent5);
			break;
			
		case R.id.kembali:
			onBackPressed();
			break;

		}
		
	}

}
