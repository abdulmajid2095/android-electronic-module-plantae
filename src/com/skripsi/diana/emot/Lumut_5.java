package com.skripsi.diana.emot;

import java.util.ArrayList;
import java.util.List;

import com.ablanco.zoomy.Zoomy;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class Lumut_5 extends Activity implements OnClickListener{

	Button jawab;
	LinearLayout utama;
	String[] jawaban={"a. Haploid","b. Diploid","c. Triploid","d. Tetraploid","e. Polyploid",
			"a. Bantuan xilem","b. Bantuan floem","c. Proses difusi","d. Proses osmosis","e. Imbibisi",
			"a.	Tumbuhan bertalus","b. Tumbuhan berkormus","c. Tumbuhan vaskuler","d. Tumbuhan yang memiliki biji","e. Tumbuhan dengan endosperma", 
			"a.	Tanah","b. Bebatuan","c. Pohon","d. Air","e. Hutan",
			"a. Sporofit","b. Embrio","c. Zigot","d. Protonema","e. Protalium",
			"a. Sporangium tumbuhan paku","b. Sporangium tumbuhan lumut","c. Anteridium tumbuhan lumut","d. Anteridium tumbuhan paku","e. Arkegonium tumbuhan paku",
			"a. Hepaticopsida","b. Anthoceropsida","c. Psilopsida","d. Bryopsida","e. Lycopsida",
			"a. Protonema - tumbuhan lumut - anteridium","b. Protonema  protalium  anteridium","c. Protalium  protonema  anteridium","d. Protalium  spora  arkegonium","e. Protonema  spora  arkegonium",
			"a. Obat hepatitis","b. Bahan pembalut","c. Penyedia oksigen","d. Industri kertas","e. Bahan bakar",
			"a. Tumbuhan lumut","b. Protalium","c. Protonema","d. Arkegonium","e. Sporogonium",
			"a. Marchantia polymorpa","b. Anthoceros puncatus","c. Polytricum sp","d. Spagnum sp","e. Phaeoceros laevis",
			"a. Anthoceropsida, Hapaticopsida, Bryopsida","b. Anthoceropsida, Hepaticopsida, Psilopsida","c. Bryopsida, Psilopsida, Anthoceropsida","d. Bryopsida, Anthoceropsida, Lycopsida","e. Psilopsida, Hepaticopsida, Lycopsida",
			"a. Bryopsida","b. Hepaticopsida","c. Psilopsida","d. Lycopsida","e. Anthoceropsida",
			"a. Zigot","b. Gametofit","c. Sel induk spora","d. Arkegonium","e. Anteridium",
			"a. Lycopsida","b. Anthoceropsida","c. Hepaticopsida","d. Hepaticopsida","e. Equisetopsida"
};
	
	
	
	String[] soal={"1.	Gametofit lumut menghasilkan sel kelamin jantan (Anteridium) yang bersifat",
					"2.	Proses pengangkutan air oleh rizoid dilakukan dengan",
					"3.	Salah satu ciri tumbuhan lumut adalah",
					"4.	Habitat lumut gambut Spagnum sp adalah ",
					"5. Tahap haploid pada metagenesis lumut terdapat pada ",
					"6. Gigi peristom dapat ditemukan pada bagian ",
					"7. Saat seorang siswa melakukan pengamatan tumbuhan di kolam ikan, ditemukan tumbuhan dengan ciri-ciri:\n1) Tubuh berbentuk seperti mangkok\n2) Tidak berdaun\n3) Gametofitnya berbentuk sepeti payung dengan tepi berlekuk membentuk cakram/payung\nBerdasarkan ciri tumbuhan diatas, dapat dikelompokkan kedalam ",
					"8. Urutan siklus hidup tumbuhan Bryophyta pada fase gametofit adalah ",
					"9. Berikut merupakan manfaat lumut bagi kehidupan, kecuali ",
					"10. Gigi peristom terdapat pada ",
					"11. Jenis tumbuhan lumut yang digunakan sebagai obat hepatitis adalah ",
					"12. Lumut diklasifikasikan dalam 3 kelas yaitu ",
					"13. Seorang siswa menemukan tanaman dengan ciri-ciri: berbentuk seperti lumut hati tetapi  sporofitnya berbentuk kapsul memanjang seperti tanduk, mengandung kutikula. Tumbuhan tersebut dapat dikelompokkan kedalam ",
					"14. Pada tumbuhan lumut, meiosis terjadi pada ",
					"15. Phaeoceros laevis merupakan tumbuhan lumut yang dapat dikelompokkan ke dalam "
	};
	
	String[] kunci={"a","e","a","d","d",
					"b","a","a","d","e",
					"a","a","e","c","b"};
	
	
	int benar=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_lumut_5);
		
		utama = (LinearLayout)findViewById(R.id.layout_uji);
		jawab = (Button)findViewById(R.id.uji_tombol_jawab);
		jawab.setOnClickListener(this);
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		looping();
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.kembali:
			onBackPressed();
			break;
		case R.id.uji_tombol_jawab:
			int skorku = (benar*2/3)*10;
			Dataku data = Dataku.findById(Dataku.class, 1L);
			if(skorku>data.skor_lumut)
			{
				data.skor_lumut = skorku;
				data.save();
			}
			
			Intent intent = new Intent(Lumut_5.this,Skor.class);
			intent.putExtra("pilihan", "lumut");
			intent.putExtra("skor", ""+skorku);
			startActivity(intent);
			break;
			
		}
	}
	
	public void looping()
	{
		for(int l=0;l<15;l++)
		{	
		
		//Soal
		TextView namawisata = new TextView(this);
		namawisata.setText(""+soal[l]);
		namawisata.setTextSize(15);
		LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT );
		namawisata.setLayoutParams(params1);
		
		final RadioGroup rg = new RadioGroup(this);
		rg.setId(l);
		int n1=l+1;
		int n2=n1*5;
		int n3=n2-5;
		
        for(int i=0;i<5;i++){
            RadioButton rb=new RadioButton(this); // dynamically creating RadioButton and adding to RadioGroup.
            //rb.setId(R.id.);
            rb.setText(""+jawaban[n3+i]);
            rg.addView(rb);
        }
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				int childCount = group.getChildCount();
                for (int x = 0; x < childCount; x++) {
                   RadioButton btn = (RadioButton) group.getChildAt(x);
                   if (btn.getId() == checkedId) {
                	   
                	   String ans = btn.getText().toString();
                	   String ans2 = ans.subSequence(0, 1).toString();
                	   int d = rg.getId();
                	   //Toast.makeText(getActivity(), ""+d, Toast.LENGTH_LONG).show();
                	   if(ans2.equals(kunci[d]))
                	   {
                		   benar=benar+1;
                	   }

                   }
                }
			}
		});
        
        LinearLayout lay1 = new LinearLayout(this);
        lay1.setBackgroundColor(Color.rgb(65, 148, 92));
		LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,2);
		params2.bottomMargin=10;
		params2.topMargin=10;
		lay1.setLayoutParams(params2);
       
		
		utama.addView(namawisata);
		utama.addView(rg);
		utama.addView(lay1);
		
		}
	}


}
