package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.text.Html;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Angio_2 extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_angio_2);
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom1);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		
		
		LinearLayout layout2 = (LinearLayout)findViewById(R.id.zoom2);
		Zoomy.Builder builder2 = new Zoomy.Builder(this)
			.target(layout2);
		builder2.register();
		
		LinearLayout layout3 = (LinearLayout)findViewById(R.id.zoom3);
		Zoomy.Builder builder3 = new Zoomy.Builder(this)
			.target(layout3);
		builder3.register();
		
		
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		Button play = (Button)findViewById(R.id.play);
		play.setOnClickListener(this);
		
		TextView teks1 = (TextView)findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("Dihasilkan 8 inti (nukelus): 1 ovum (n), 2 sinergid (n), 3 antipoda (n), dan 2 inti polar yang bersatu disebut inti <b>kandung lembaga sekunder</b>."));
		TextView teks2 = (TextView)findViewById(R.id.teks2);
		teks2.setText(Html.fromHtml("Pembuahan (fertilisasi) Angiospermae disebut <b>pembuahan ganda</b>."));
	
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.kembali:
			onBackPressed();
			break;
		case R.id.play:
			Intent intent = new Intent(Angio_2.this,Video_Angiospermae.class);
			startActivity(intent);
			break;

		}
	}

	

}
