package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Materi extends Activity implements OnClickListener{

	Button lanjut,berpembuluh,tidak_berpembuluh;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_materi);
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		/*lanjut = (Button) findViewById(R.id.lanjut);
		lanjut.setOnClickListener(this);*/
		berpembuluh = (Button) findViewById(R.id.materi_berpembuluh);
		berpembuluh.setOnClickListener(this);
		tidak_berpembuluh = (Button) findViewById(R.id.materi_tidak_berpembuluh);
		tidak_berpembuluh.setOnClickListener(this);
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		/*case R.id.lanjut:
			Intent kingdom = new Intent(Materi.this,Materi_Jenis.class);
			startActivity(kingdom);
			break;*/
		case R.id.materi_berpembuluh:
			Intent kingdom = new Intent(Materi.this,Materi_Jenis.class);
			startActivity(kingdom);
			break;
		case R.id.materi_tidak_berpembuluh:
			Intent kingdom2 = new Intent(Materi.this,Materi2.class);
			startActivity(kingdom2);
			break;
		case R.id.kembali:
			onBackPressed();
			break;

		}
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		Intent intent = new Intent(Materi.this,MenuUtama.class);
		startActivity(intent);
	}

}
