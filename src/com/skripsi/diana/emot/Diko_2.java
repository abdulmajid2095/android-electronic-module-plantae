package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Diko_2 extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_diko_2);
		
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
		Button diko1=(Button)findViewById(R.id.diko_1);
		diko1.setOnClickListener(this);
		Button diko2=(Button)findViewById(R.id.diko_2);
		diko2.setOnClickListener(this);
		Button diko3=(Button)findViewById(R.id.diko_3);
		diko3.setOnClickListener(this);
		Button diko4=(Button)findViewById(R.id.diko_4);
		diko4.setOnClickListener(this);
		Button diko5=(Button)findViewById(R.id.diko_5);
		diko5.setOnClickListener(this);
		Button diko6=(Button)findViewById(R.id.diko_6);
		diko6.setOnClickListener(this);
		Button diko7=(Button)findViewById(R.id.diko_7);
		diko7.setOnClickListener(this);
		Button diko8=(Button)findViewById(R.id.diko_8);
		diko8.setOnClickListener(this);
		Button diko9=(Button)findViewById(R.id.diko_9);
		diko9.setOnClickListener(this);
		Button diko10=(Button)findViewById(R.id.diko_10);
		diko10.setOnClickListener(this);
		Button diko11=(Button)findViewById(R.id.diko_11);
		diko11.setOnClickListener(this);
		Button diko12=(Button)findViewById(R.id.diko_12);
		diko12.setOnClickListener(this);
		Button diko13=(Button)findViewById(R.id.diko_13);
		diko13.setOnClickListener(this);
		
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.kembali:
			onBackPressed();
			break;
			
		case R.id.diko_1:
			Intent intent1 = new Intent(Diko_2.this,Diko_2a.class);
			startActivity(intent1);
			break;
		case R.id.diko_2:
			Intent intent2 = new Intent(Diko_2.this,Diko_2b.class);
			startActivity(intent2);
			break;
		case R.id.diko_3:
			Intent intent3 = new Intent(Diko_2.this,Diko_2c.class);
			startActivity(intent3);
			break;
		case R.id.diko_4:
			Intent intent4 = new Intent(Diko_2.this,Diko_2d.class);
			startActivity(intent4);
			break;
		case R.id.diko_5:
			Intent intent5 = new Intent(Diko_2.this,Diko_2e.class);
			startActivity(intent5);
			break;
		case R.id.diko_6:
			Intent intent6 = new Intent(Diko_2.this,Diko_2f.class);
			startActivity(intent6);
			break;
		case R.id.diko_7:
			Intent intent7 = new Intent(Diko_2.this,Diko_2g.class);
			startActivity(intent7);
			break;
		case R.id.diko_8:
			Intent intent8 = new Intent(Diko_2.this,Diko_2h.class);
			startActivity(intent8);
			break;
		case R.id.diko_9:
			Intent intent9 = new Intent(Diko_2.this,Diko_2i.class);
			startActivity(intent9);
			break;
		case R.id.diko_10:
			Intent intent10 = new Intent(Diko_2.this,Diko_2j.class);
			startActivity(intent10);
			break;
		case R.id.diko_11:
			Intent intent11 = new Intent(Diko_2.this,Diko_2k.class);
			startActivity(intent11);
			break;
		case R.id.diko_12:
			Intent intent12 = new Intent(Diko_2.this,Diko_2l.class);
			startActivity(intent12);
			break;
		case R.id.diko_13:
			Intent intent13 = new Intent(Diko_2.this,Diko_2m.class);
			startActivity(intent13);
			break;

		}
	}

	

}
