package com.skripsi.diana.emot;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

public class Video_Angiospermae extends Activity {

	VideoView v4;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		setContentView(R.layout.activity_video__angiospermae);
		
		v4 = (VideoView)findViewById(R.id.v4);
		Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.v4);
		v4.setVideoURI(uri);
		v4.setMediaController(new MediaController(this));
		v4.requestFocus();
		v4.start();
	}

	

}
