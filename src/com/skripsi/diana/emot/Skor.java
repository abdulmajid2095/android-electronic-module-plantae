package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class Skor extends Activity implements OnClickListener{

	Button pembahasan,kembali;
	TextView skorku;
	int benar;
	Dataku data;
	String pilihan,skor;
	String p1="lumut",p2="paku",p3="gymno";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_skor);
		
		kembali=(Button)findViewById(R.id.skor_kembali);
		kembali.setOnClickListener(this);
		pembahasan=(Button)findViewById(R.id.skor_pembahasan);
		pembahasan.setOnClickListener(this);
		
		Intent baru = getIntent();
		pilihan = baru.getStringExtra("pilihan");
		skor = baru.getStringExtra("skor");
		
		data = Dataku.findById(Dataku.class, 1L);
		skorku = (TextView)findViewById(R.id.skorku);
		skorku.setText(""+skor);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.skor_kembali:
			onBackPressed();
			break;

		case R.id.skor_pembahasan:
			if(pilihan.equals(p1))
			{
				Intent intent = new Intent(Skor.this,Pembahasan_lumut.class);
				startActivity(intent);
			}
			else if(pilihan.equals(p2))
			{
				Intent intent2 = new Intent(Skor.this,Pembahasan_Paku.class);
				startActivity(intent2);
			}
			else if(pilihan.equals(p3))
			{
				Intent intent3 = new Intent(Skor.this,Pembahasan_Gymno.class);
				startActivity(intent3);
			}
			else
			{
				Intent intent4 = new Intent(Skor.this,Pembahasan_Angio.class);
				startActivity(intent4);
			}
			break;
		
	}
	}
}
