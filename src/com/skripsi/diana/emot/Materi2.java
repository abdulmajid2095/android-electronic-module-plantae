package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Materi2 extends Activity implements OnClickListener{

	Button lumut_1,lumut_2,lumut_3,lumut_4,lumut_5;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_materi2);
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		lumut_1=(Button)findViewById(R.id.lumut__x_ciri);
		lumut_1.setOnClickListener(this);
		lumut_2=(Button)findViewById(R.id.lumut_x_siklus);
		lumut_2.setOnClickListener(this);
		lumut_3=(Button)findViewById(R.id.lumut_x_klasifikasi);
		lumut_3.setOnClickListener(this);
		lumut_4=(Button)findViewById(R.id.lumut_x_peran);
		lumut_4.setOnClickListener(this);
		lumut_5=(Button)findViewById(R.id.lumut_x_uji);
		lumut_5.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		
		case R.id.lumut__x_ciri:
			Intent intent1 = new Intent(Materi2.this,Lumut_1.class);
			startActivity(intent1);
			break;
		case R.id.lumut_x_siklus:
			Intent intent2 = new Intent(Materi2.this,Lumut_2.class);
			startActivity(intent2);
			break;
		case R.id.lumut_x_klasifikasi:
			Intent intent3 = new Intent(Materi2.this,Lumut_3.class);
			startActivity(intent3);
			break;
		case R.id.lumut_x_peran:
			Intent intent4 = new Intent(Materi2.this,Lumut_4.class);
			startActivity(intent4);
			break;
		case R.id.lumut_x_uji:
			Intent intent5 = new Intent(Materi2.this,Lumut_5.class);
			startActivity(intent5);
			break;
			
		case R.id.kembali:
			onBackPressed();
			break;

		}
		
	}

}
