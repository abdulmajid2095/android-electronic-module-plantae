package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Materi5 extends Activity implements OnClickListener{

	Button gymno_1,gymno_2,gymno_3,gymno_4,gymno_5;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_materi5);
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
		gymno_1=(Button)findViewById(R.id.gymno__x_ciri);
		gymno_1.setOnClickListener(this);
		gymno_2=(Button)findViewById(R.id.gymno_x_siklus);
		gymno_2.setOnClickListener(this);
		gymno_3=(Button)findViewById(R.id.gymno_x_klasifikasi);
		gymno_3.setOnClickListener(this);
		gymno_4=(Button)findViewById(R.id.gymno_x_peran);
		gymno_4.setOnClickListener(this);
		gymno_5=(Button)findViewById(R.id.gymno_x_uji);
		gymno_5.setOnClickListener(this);
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		
		case R.id.gymno__x_ciri:
			Intent intent1 = new Intent(Materi5.this,Gymno_1.class);
			startActivity(intent1);
			break;
		case R.id.gymno_x_siklus:
			Intent intent2 = new Intent(Materi5.this,Gymno_2.class);
			startActivity(intent2);
			break;
		case R.id.gymno_x_klasifikasi:
			Intent intent3 = new Intent(Materi5.this,Gymno_3.class);
			startActivity(intent3);
			break;
		case R.id.gymno_x_peran:
			Intent intent4 = new Intent(Materi5.this,Gymno_4.class);
			startActivity(intent4);
			break;
		case R.id.gymno_x_uji:
			Intent intent5 = new Intent(Materi5.this,Gymno_5.class);
			startActivity(intent5);
			break;
			
		case R.id.kembali:
			onBackPressed();
			break;

		}
		
	}

}
