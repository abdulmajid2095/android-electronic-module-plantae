package com.skripsi.diana.emot;



import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class Adapter_dikotil_tab extends FragmentPagerAdapter {

	public Adapter_dikotil_tab(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int index) {
		switch (index) {
        case 0:
            // Top Rated fragment activity
            return new Dikotil_Ciri();
        case 1:
            // Games fragment activity
        	return new Dikotil_Ordo();
        
        }
		
 
        return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 2;
	}
	
	@Override
    public CharSequence getPageTitle(int position) {
        //return "Page #" + ( position + 1 );
    	switch (position) {
        case 0:
            // Top Rated fragment activity
        	return "Tentang Tumbuhan Dikotil";
        case 1:
            // Games fragment activity
        	return "Ordo";
        
        }
 
        return null;
    }

}
