package com.skripsi.diana.emot;



import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class Adapter_spermatophyta_tab extends FragmentPagerAdapter {

	public Adapter_spermatophyta_tab(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int index) {
		switch (index) {
        case 0:
            // Top Rated fragment activity
            return new Spermatophyta_Pengertian();
        case 1:
            // Games fragment activity
        	return new Spermatophyta_Ciri();
        case 2:
            // Games fragment activity
        	return new Spermatophyta_Struktur();
        case 3:
            // Games fragment activity
        	return new Spermatophyta_Divisi();
        case 4:
            // Games fragment activity
        	return new Spermatophyta_Divisi();
        
        }
		
 
        return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 5;
	}
	
	@Override
    public CharSequence getPageTitle(int position) {
        //return "Page #" + ( position + 1 );
    	switch (position) {
        case 0:
            // Top Rated fragment activity
        	return "Tentang Spermatophyta";
        case 1:
            // Games fragment activity
        	return "Ciri-ciri";
        case 2:
            // Games fragment activity
        	return "Struktur dan fungsi";
        case 3:
            // Games fragment activity
        	return "Pembagian Divisi";
        case 4:
            // Games fragment activity
        	return "Uji Kompetensi";
        
        }
 
        return null;
    }

}
