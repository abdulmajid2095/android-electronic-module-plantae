package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class Spermatophyta_Divisi extends Fragment implements OnClickListener{

	Button detail_gymnospermae,detail_angiospermae;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_spermatophyta__divisi, container, false);
		detail_gymnospermae = (Button) rootView.findViewById(R.id.spermathophyta_divisi_gymnospermae);
		detail_gymnospermae.setOnClickListener(this);
		detail_angiospermae = (Button) rootView.findViewById(R.id.spermathophyta_divisi_angiospermae);
		detail_angiospermae.setOnClickListener(this);
		return rootView;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.spermathophyta_divisi_angiospermae:
			Intent intent1 = new Intent(getActivity(),Materi_Angiospermae.class);
			startActivity(intent1);
			break;
		case R.id.spermathophyta_divisi_gymnospermae:
			Intent intent2 = new Intent(getActivity(),Materi_Gymnospermae.class);
			startActivity(intent2);
			break;

		}
	}


}
