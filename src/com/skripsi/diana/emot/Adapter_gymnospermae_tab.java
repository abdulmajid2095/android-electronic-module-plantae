package com.skripsi.diana.emot;



import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class Adapter_gymnospermae_tab extends FragmentPagerAdapter {

	public Adapter_gymnospermae_tab(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int index) {
		switch (index) {
        case 0:
            // Top Rated fragment activity
            return new Gymnospermae_Pengertian();
        case 1:
            // Games fragment activity
        	return new Gymnospermae_Siklus();
        case 2:
            // Games fragment activity
        	return new Gymnospermae_Klasifikasi();
        case 3:
            // Games fragment activity
        	return new Gymnospermae_Manfaat();
        case 4:
            // Games fragment activity
        	return new Gymnospermae_Video();
        
        }
		
 
        return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 5;
	}
	
	@Override
    public CharSequence getPageTitle(int position) {
        //return "Page #" + ( position + 1 );
    	switch (position) {
        case 0:
            // Top Rated fragment activity
        	return "Tentang Gymnospermae";
        case 1:
            // Games fragment activity
        	return "Siklus Hidup";
        case 2:
            // Games fragment activity
        	return "Klasifikasi";
        case 3:
            // Games fragment activity
        	return "Manfaat";
        case 4:
            // Games fragment activity
        	return "Video";
        
        }
 
        return null;
    }

}
