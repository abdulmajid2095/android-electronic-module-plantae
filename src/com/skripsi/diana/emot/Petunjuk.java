package com.skripsi.diana.emot;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class Petunjuk extends Activity implements OnClickListener{

	Button lanjut;
	MediaPlayer musik3;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_petunjuk);
		
		lanjut = (Button)findViewById(R.id.petunjuk_lanjut);
		lanjut.setOnClickListener(this);
		
		musik3 = MediaPlayer.create(this, R.raw.tutorial);
		musik3.setLooping(true);
        musik3.setVolume(1,1);
        musik3.start();
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.petunjuk_lanjut:
			musik3.stop();
			musik3.release();
			musik3 = null;
			
			Intent intent = new Intent(Petunjuk.this,MenuUtama.class);
			startActivity(intent);
			break;

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
	}

}
