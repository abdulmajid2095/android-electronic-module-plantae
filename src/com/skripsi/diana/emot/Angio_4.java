package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.text.Html;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Angio_4 extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_angio_4);
		
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
		TextView teks1 = (TextView)findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("Bahan makanan pokok (padi, jagung, umbi jalar, singkong)"));
		TextView teks2 = (TextView)findViewById(R.id.teks2);
		teks2.setText(Html.fromHtml("Bahan sayuran (bayam, kangkung, kacang panjang)"));
		TextView teks3 = (TextView)findViewById(R.id.teks3);
		teks3.setText(Html.fromHtml("Bahan oabt-obatan (kunyit, jahe, sambiloto, adas, dll)"));
		
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.kembali:
			onBackPressed();
			break;

		}
	}

	

}
