package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.text.Html;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Paku_4 extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_paku_4);
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		
		TextView teks1 = (TextView)findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("<i>Equisetum sp</i> sebagai bahan penggosok."));
		
		TextView teks2 = (TextView)findViewById(R.id.teks2);
		teks2.setText(Html.fromHtml("<i>Selaginella plana</i> sebagai obat luka."));
		
		TextView teks3 = (TextView)findViewById(R.id.teks3);
		teks3.setText(Html.fromHtml("<i>Marsilea crenata</i> (semanggi) sebagai sayuran."));
		
		TextView teks4 = (TextView)findViewById(R.id.teks4);
		teks4.setText(Html.fromHtml("<i>Azolla pinnata</i> sebagai pupuk hijau"));
		
		TextView teks5 = (TextView)findViewById(R.id.teks5);
		teks5.setText(Html.fromHtml("<i>Asplenium nidus, Adiantum sp</i> Sebagai tanaman hias"));
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.kembali:
			onBackPressed();
			break;

		}
	}

	

}
