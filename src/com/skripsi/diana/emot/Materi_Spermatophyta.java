package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

public class Materi_Spermatophyta extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_materi__spermatophyta);
		
		ViewPager pager = (ViewPager) findViewById(R.id.pager_spermatophyta);
        FragmentManager fm = getSupportFragmentManager();
        Adapter_spermatophyta_tab pagerAdapter = new Adapter_spermatophyta_tab(fm);
        pager.setAdapter(pagerAdapter);
	}


}
