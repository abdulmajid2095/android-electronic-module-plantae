package com.skripsi.diana.emot;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Paku_Peranan extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_paku__peranan, container, false);
		
		TextView teks1 = (TextView)rootView.findViewById(R.id.teks1);
		teks1.setText(Html.fromHtml("Bahan penggosok (ampelas) misalnya <i>Equisetum sp</i>."));
		
		TextView teks2 = (TextView)rootView.findViewById(R.id.teks2);
		teks2.setText(Html.fromHtml("Bahan dasar pembuatan obat-obatan. Contohnya: <i>Equisetum</i> memiliki fungsi diuretik (melancarkan pengeluaran urin) dan <i>Selaginella plana</i> (obat luka)."));
		
		TextView teks3 = (TextView)rootView.findViewById(R.id.teks3);
		teks3.setText(Html.fromHtml("Bahan makanan, terutama sebagai sayuran. Contohnya : <i>Marsilea crenata</i> (semanggi)."));
		
		TextView teks4 = (TextView)rootView.findViewById(R.id.teks4);
		teks4.setText(Html.fromHtml("Sebagai pupuk hijau. Contohnya : <i>Azolla pinnata</i> yang hidup di sawah-sawah, jenis paku ini bersimbiosis dengan  <i>Anabaena azollae</i>, yaitu sejenis ganggang biru yang berkemampuan untuk  memfiksasi N2 bebas di udara."));
		
		TextView teks5 = (TextView)rootView.findViewById(R.id.teks5);
		teks5.setText(Html.fromHtml("Sebagai tanaman hias. Contohnya: <i>Asplenium nidus, adiantum.</i>"));
		
		return rootView;
	}


}
