package com.skripsi.diana.emot;

import com.ablanco.zoomy.Zoomy;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class Materi4 extends Activity implements OnClickListener{

	Button lanjut,gymno,angio;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_materi4);
		Button kembali = (Button)findViewById(R.id.kembali);
		kembali.setOnClickListener(this);
		lanjut = (Button) findViewById(R.id.tbl_uji_sper);
		lanjut.setOnClickListener(this);
		gymno = (Button) findViewById(R.id.biji_gymno);
		gymno.setOnClickListener(this);
		angio = (Button) findViewById(R.id.biji_angio);
		angio.setOnClickListener(this);
		LinearLayout layout = (LinearLayout)findViewById(R.id.zoom);
		Zoomy.Builder builder = new Zoomy.Builder(this)
			.target(layout);
		builder.register();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.tbl_uji_sper:
			Intent king = new Intent(Materi4.this,Gymno_5.class);
			startActivity(king);
			break;
		case R.id.biji_gymno:
			Intent kingdom = new Intent(Materi4.this,Materi5.class);
			startActivity(kingdom);
			break;
		case R.id.biji_angio:
			Intent kingdom2 = new Intent(Materi4.this,Materi6.class);
			startActivity(kingdom2);
			break;
		case R.id.kembali:
			onBackPressed();
			break;

		}
	}
	
}
